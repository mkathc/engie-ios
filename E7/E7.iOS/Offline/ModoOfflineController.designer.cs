// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace E7.iOS
{
    [Register ("ModoOfflineController")]
    partial class ModoOfflineController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton btnSincronizar { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel lblMes { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel lblUltimaSincronizacion { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField pickerTextFieldMeses { get; set; }

        [Action ("BtnSincronizar_TouchUpInside:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void BtnSincronizar_TouchUpInside (UIKit.UIButton sender);

        void ReleaseDesignerOutlets ()
        {
            if (btnSincronizar != null) {
                btnSincronizar.Dispose ();
                btnSincronizar = null;
            }

            if (lblMes != null) {
                lblMes.Dispose ();
                lblMes = null;
            }

            if (lblUltimaSincronizacion != null) {
                lblUltimaSincronizacion.Dispose ();
                lblUltimaSincronizacion = null;
            }

            if (pickerTextFieldMeses != null) {
                pickerTextFieldMeses.Dispose ();
                pickerTextFieldMeses = null;
            }
        }
    }
}