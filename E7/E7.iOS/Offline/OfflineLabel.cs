﻿using System;
using UIKit;

namespace E7.iOS.Offline
{
    public class OfflineLabel
    {
        public static UIView Label(nfloat width, nfloat height, bool red)
        {
            CoreGraphics.CGRect cGRect = new CoreGraphics.CGRect(
                (width / 5) * 5,
                (height / 3) * 1,
                60,
                20);

            UILabel txt = new UILabel(cGRect);
            txt.Text = "  Modo Offline  ";
            if (red)
				txt.TextColor = Application.Rojo;
            else
                txt.TextColor = Application.Azul;
            txt.AdjustsFontSizeToFitWidth = true;
            txt.Font = UIFont.BoldSystemFontOfSize(8f);
            txt.Center = new CoreGraphics.CGPoint(cGRect.Width / 2, cGRect.Height / 2);

            UIView view = new UIView(cGRect);
            view.BackgroundColor = UIColor.White;
            view.AddSubview(txt);

            return view;
        }
    }
}
