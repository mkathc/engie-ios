// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace E7.iOS
{
    [Register ("MenuController")]
    partial class MenuController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton CerrarSesionButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton ConsumoButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton FacturacionButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton InicioButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton ModoOfflineButton { get; set; }

        [Action ("CerrarSesionButton_TouchUpInside:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void CerrarSesionButton_TouchUpInside (UIKit.UIButton sender);

        [Action ("ConsumoButton_TouchUpInside:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void ConsumoButton_TouchUpInside (UIKit.UIButton sender);

        [Action ("FacturacionButton_TouchUpInside:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void FacturacionButton_TouchUpInside (UIKit.UIButton sender);

        [Action ("InicioButton_TouchUpInside:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void InicioButton_TouchUpInside (UIKit.UIButton sender);

        [Action ("ModoOfflineButton_TouchUpInside:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void ModoOfflineButton_TouchUpInside (UIKit.UIButton sender);

        void ReleaseDesignerOutlets ()
        {
            if (CerrarSesionButton != null) {
                CerrarSesionButton.Dispose ();
                CerrarSesionButton = null;
            }

            if (ConsumoButton != null) {
                ConsumoButton.Dispose ();
                ConsumoButton = null;
            }

            if (FacturacionButton != null) {
                FacturacionButton.Dispose ();
                FacturacionButton = null;
            }

            if (InicioButton != null) {
                InicioButton.Dispose ();
                InicioButton = null;
            }

            if (ModoOfflineButton != null) {
                ModoOfflineButton.Dispose ();
                ModoOfflineButton = null;
            }
        }
    }
}