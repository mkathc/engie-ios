﻿using System;

using UIKit;

namespace E7.iOS
{
    public partial class ContentController : UIViewController
    {
        public ContentController() : base("ContentController", null)
        {
        }

        public ContentController(IntPtr b) : base(b) {}

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            // Perform any additional setup after loading the view, typically from a nib.
        }

        public override void DidReceiveMemoryWarning()
        {
            base.DidReceiveMemoryWarning();
            // Release any cached data, images, etc that aren't in use.
        }
    }
}

