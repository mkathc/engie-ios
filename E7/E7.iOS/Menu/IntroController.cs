﻿using System;
using UIKit;

namespace E7.iOS
{
    public partial class IntroController : BaseController//UIViewController
    {
        //public IntroController() : base("IntroController", null)
        //{
        //}

        public IntroController(IntPtr b) : base(b) {}

        // provide access to the sidebar controller to all inheriting controllers
        protected SidebarNavigation.SidebarController SidebarController
        {
            get
            {
                return (UIApplication.SharedApplication.Delegate as AppDelegate).RootViewController.SidebarController;
            }
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            // Perform any additional setup after loading the view, typically from a nib.

            var main = Application.GetInstance();
            lblUsuario.Text = main.nombreCompleto;
        }

        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);
            UIImageView img = new UIImageView(UIImage.FromFile("barra_mylink2.png"));
            NavigationItem.TitleView = img;
            NavigationController.NavigationBar.SetBackgroundImage(new UIImage(), UIBarMetrics.Default);
            NavigationController.NavigationBar.ShadowImage = new UIImage();
			NavigationController.NavigationBar.Translucent = true;
        }

        public override void DidReceiveMemoryWarning()
        {
            base.DidReceiveMemoryWarning();
            // Release any cached data, images, etc that aren't in use.
        }
    }
}
