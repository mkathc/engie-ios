﻿using System;
using System.Collections.Generic;
using System.Linq;
using E7.Models;
using Foundation;
using UIKit;

namespace E7.iOS
{
	public class Application
	{
        public static string AppName = "Engie";
        public string usuarioInterno { get; set; }
        public string nombreCompleto { get; set; }
        public List<Clientes> clientes { get; set; }
        public ConsumoFacturadoModel consumoFacturado { get; set; }
        public MedicionGraficaModel medicionGrafica { get; set; }
        public DistribucionFacturadaModel distribucionFacturada { get; set; }
        public GraficoHistoricoFacturadoModel graficoHistoricoFacturado { get; set; }
        public GraficoHistoricoFacturadoModel graficoHistoricoPrecios { get; set; }
        public static List<string> Monedas { get; set; }
        public static List<string> Meses { get; set; }
        public static List<string> Anios { get; set; }
        public static List<string> Dias { get; set; }
        public static UIColor Azul { get; set; }
        public static UIColor Rojo { get; set; }
        public static UIColor Verde { get; set; }
		public static string Usuario { get; set; }
        public static string Codigo { get; set; }
        public string ModoOff { get; set; }
        public string UltimaSincronizacion { get; set; }

        static Application instance;

        // This is the main entry point of the application.
		static void Main (string[] args)
		{
			// if you want to use a different Application Delegate class from "AppDelegate"
			// you can specify it here.

            UIApplication.Main (args, null, "AppDelegate");
		}

        public Application()
        {
            instance = this;
            Monedas = new List<string>();
			Meses = new List<string>();
            Anios = new List<string>();
            Dias = new List<string>();
            Azul = UIColor.FromRGB(9, 91, 156);
            Rojo = UIColor.FromRGB(164, 40, 34);
            Verde = UIColor.FromRGB(26, 163, 86);

			Monedas.Add("USD");
            Monedas.Add("PEN");

            Meses.Add("Ene");
            Meses.Add("Feb");
            Meses.Add("Mar");
            Meses.Add("Abr");
            Meses.Add("May");
            Meses.Add("Jun");
            Meses.Add("Jul");
            Meses.Add("Ago");
            Meses.Add("Sep");
            Meses.Add("Oct");
            Meses.Add("Nov");
            Meses.Add("Dic");

            var anio = DateTime.Now.Year;
            for (int i = anio; i >= 2014; i--)
                Anios.Add(i.ToString());

			for (var i = 1; i <= 31; i++)
            {
                var j = i.ToString();
                j = (j.Length == 1) ? "0" + j : j ;
                Dias.Add(j);
            }
        }

        public static Application GetInstance()
        {
            if (instance == null)
            {
                return new Application();
            }

            return instance;
        }
	}
}
