﻿using System;
using System.Collections.Generic;
using UIKit;

namespace E7.iOS
{
    public partial class PickerDataModel : UIPickerViewModel
    {
        int selectedIndex;

        public event EventHandler<EventArgs> ValueChanged;

        public List<string> Items { get; private set; }

        public string SelectedItem
        {
            get 
            {
                try
                {
                    return Items[selectedIndex];
                }
                catch
                {
                    return null;
                }
            }
        }

        public int SelectedIndex 
        {
            get
            {
                return selectedIndex;
            }
            set
            {
                selectedIndex = value;
            }
        }

        public PickerDataModel()
        {
            Items = new List<string>();
        }

        public override nint GetRowsInComponent(UIPickerView picker, nint component)
        {
            return Items.Count;
        }

        public override string GetTitle(UIPickerView picker, nint row, nint component)
        {
            return Items[(int)row];
        }

        public override nint GetComponentCount(UIPickerView picker)
        {
            return 1;
        }

        public override void Selected(UIPickerView picker, nint row, nint component)
        {
            selectedIndex = (int)row;
            if (ValueChanged != null)
            {
                ValueChanged(this, new EventArgs());
            }
        }
    }
}

