using Foundation;
using System;
using UIKit;
using SciChart.iOS.Charting;

namespace E7.iOS
{
    public partial class DistribucionFacturadaEReactiva : UIViewController
    {
        SCIChartSurface _surface;
        SCIVerticallyStackedMountainsCollection _mountainCollection;

        public DistribucionFacturadaEReactiva() : base("DistribucionFacturadaEReactiva", null)
        {
        }

        public DistribucionFacturadaEReactiva(IntPtr b) : base(b) { }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            // Perform any additional setup after loading the view, typically from a nib.
        }

        public override void ViewDidAppear(bool animated)
        {
            base.ViewDidAppear(animated);

            _surface = new SCIChartSurface();

            _surface.TranslatesAutoresizingMaskIntoConstraints = true;
            _surface.Frame = this.View.Bounds;

            this.View.AddSubview(_surface);

            _surface.XAxes.Add(new SCINumericAxis());
            _surface.YAxes.Add(new SCINumericAxis());

            var ds1 = new XyDataSeries<double, double> { SeriesName = "data 1" };
            ds1.Append(1.0, 0.8);
            ds1.Append(2.0, 0.4);
            ds1.Append(3.0, 0.9);
            ds1.Append(4.0, 0.2);
            ds1.Append(5.0, 0.3);
            ds1.Append(6.0, 0.6);
            ds1.Append(7.0, 0.1);

            var series1 = GetRenderableSeries(ds1);

            _mountainCollection = new SCIVerticallyStackedMountainsCollection();
            _mountainCollection.Add(series1);

            _surface.RenderableSeries.Add(_mountainCollection);
            _surface.ChartModifiers.Add(new SCICursorModifier());
        }

        public override void DidReceiveMemoryWarning()
        {
            base.DidReceiveMemoryWarning();
            // Release any cached data, images, etc that aren't in use.
        }

        private SCIStackedMountainRenderableSeries GetRenderableSeries(IDataSeries dataSeries)
        {
            return new SCIStackedMountainRenderableSeries
            {
                DataSeries = dataSeries,
            };
        }
    }
}