﻿using System;
using E7.Services;
using E7.iOS.Utils;
using UIKit;
using E7.Models;
using System.Collections.Generic;

namespace E7.iOS
{
    public partial class DistribucionFacturadaController : UIViewController
    {
        PickerDataModel _pickerDataClientes, _pickerDataFacturacion, _pickerDataAnio, _pickerDataMes;
        UIPickerView _pickerViewClientes, _pickerViewFacturacion, _pickerViewAnio, _pickerViewMes;
        Application _main;

        public DistribucionFacturadaController() : base("DistribucionFacturadaController", null)
        {
        }

        public DistribucionFacturadaController(IntPtr b) : base(b) { }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            // Perform any additional setup after loading the view, typically from a nib.

            if (this.View.Frame.Height <= 480)
            {
                NSLayoutConstraint.Create(txtTitle, NSLayoutAttribute.Top,
                                            NSLayoutRelation.Equal, this.View, NSLayoutAttribute.Top,
                                            1, 82).Active = true;
                NSLayoutConstraint.Create(stackPrincipalBg, NSLayoutAttribute.Top,
                                          NSLayoutRelation.Equal, this.View, NSLayoutAttribute.Top,
                                          1, 115).Active = true;
                NSLayoutConstraint.Create(stackPrincipal, NSLayoutAttribute.Top,
                                            NSLayoutRelation.Equal, this.View, NSLayoutAttribute.Top,
                                            1, 125).Active = true;
                lblClientes.Font = lblClientes.Font.WithSize(10);
                lblFacturacion.Font = lblClientes.Font.WithSize(10);
                lblFecha.Font = lblClientes.Font.WithSize(10);
            }

            _main = Application.GetInstance();
            _main.distribucionFacturada = new DistribucionFacturadaModel();
            GetClientes();
        }

        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);
            NavigationController.NavigationBar.Translucent = true;
        }

        void _pickerDataClientes_ValueChanged(object sender, EventArgs e)
        {
            pickerTextFieldClientes.Text = _pickerDataClientes.SelectedItem;
            _main.distribucionFacturada.Cliente = _pickerDataClientes.SelectedItem;

            _pickerDataFacturacion.Items.Clear();
            foreach (var x in _main.clientes)
            {
                if (x.NomCliente == _pickerDataClientes.SelectedItem)
                {
                    _main.distribucionFacturada.ClienteID = int.Parse(x.CodCliente);
					_main.distribucionFacturada.PuntoFID = 0;
                    foreach (var y in x.PFaCliente)
                    {
                        _pickerDataFacturacion.Items.Add(y.Descripcion);
                    }
                }
            }
            _pickerDataFacturacion.SelectedIndex = 0;
            pickerTextFieldFacturacion.Text = _pickerDataFacturacion.SelectedItem;
        }

        void _pickerDataFacturacion_ValueChanged(object sender, EventArgs e)
        {
            pickerTextFieldFacturacion.Text = _pickerDataFacturacion.SelectedItem;
            _main.distribucionFacturada.PuntoF = _pickerDataFacturacion.SelectedItem;

            foreach (var x in _main.clientes)
                foreach (var y in x.PFaCliente)
                    if (y.Descripcion == _pickerDataFacturacion.SelectedItem)
                        _main.distribucionFacturada.PuntoFID = int.Parse(y.ID);
        }

        void _pickerDataMes_ValueChanged(object sender, EventArgs e)
        {
            pickerTextFieldMes.Text = _pickerDataMes.SelectedItem;
            ConsultaDistribucionFacturada.Month = pickerTextFieldMes.Text;
            _main.distribucionFacturada.Mes = DatesHelper.GetMonthKey(_pickerDataMes.SelectedItem);
        }

        void _pickerDataAnio_ValueChanged(object sender, EventArgs e)
        {
            pickerTextFieldAnio.Text = _pickerDataAnio.SelectedItem;
            ConsultaDistribucionFacturada.Year = pickerTextFieldAnio.Text;
            _main.distribucionFacturada.Anio = _pickerDataAnio.SelectedItem;
        }

        public override void DidReceiveMemoryWarning()
        {
            base.DidReceiveMemoryWarning();
            // Release any cached data, images, etc that aren't in use.
        }

        partial void UIButton32816_TouchUpInside(UIButton sender)
        {
            NavigationItem.BackBarButtonItem = new UIBarButtonItem("", UIBarButtonItemStyle.Plain, null);
        }

        void GetClientes()
        {
            var main = Application.GetInstance();
            var clientes = main.clientes;

            _pickerDataClientes = new PickerDataModel();
            _pickerDataFacturacion = new PickerDataModel();
            _pickerDataMes = new PickerDataModel();
            _pickerDataAnio = new PickerDataModel();

            foreach (var c in clientes)
                _pickerDataClientes.Items.Add(c.NomCliente);

            if (clientes[0] != null)
                foreach (var x in clientes[0].PFaCliente)
                    _pickerDataFacturacion.Items.Add(x.Descripcion);

            foreach (var x in Application.Meses)
                _pickerDataMes.Items.Add(x);
            
            foreach (var x in Application.Anios)
                _pickerDataAnio.Items.Add(x);
            
            _pickerViewClientes = new TextField(pickerTextFieldClientes);
			_pickerDataClientes.ValueChanged += _pickerDataClientes_ValueChanged;
			_pickerViewClientes.Model = _pickerDataClientes;
            pickerTextFieldClientes.Text = _pickerDataClientes.SelectedItem;
            pickerTextFieldClientes.InputView = _pickerViewClientes;

            _pickerViewFacturacion = new TextField(pickerTextFieldFacturacion);
			_pickerDataFacturacion.ValueChanged += _pickerDataFacturacion_ValueChanged;
			_pickerViewFacturacion.Model = _pickerDataFacturacion;
            pickerTextFieldFacturacion.Text = _pickerDataFacturacion.SelectedItem;
            pickerTextFieldFacturacion.InputView = _pickerViewFacturacion;

            _pickerViewMes = new TextField(pickerTextFieldMes);
			_pickerDataMes.ValueChanged += _pickerDataMes_ValueChanged;
			_pickerViewMes.Model = _pickerDataMes;
            pickerTextFieldMes.Text = _pickerDataMes.SelectedItem;
            pickerTextFieldMes.InputView = _pickerViewMes;
            ConsultaDistribucionFacturada.Month = pickerTextFieldMes.Text;

            _pickerViewAnio = new TextField(pickerTextFieldAnio);
			_pickerDataAnio.ValueChanged += _pickerDataAnio_ValueChanged;
			_pickerViewAnio.Model = _pickerDataAnio;
            pickerTextFieldAnio.Text = _pickerDataAnio.SelectedItem;
            pickerTextFieldAnio.InputView = _pickerViewAnio;
            ConsultaDistribucionFacturada.Year = pickerTextFieldAnio.Text;

            _main.distribucionFacturada.ClienteID = int.Parse((clientes[0] != null) ? clientes[0].CodCliente : "0");
            _main.distribucionFacturada.PuntoFID = int.Parse((clientes[0].PFaCliente[0] != null) ? clientes[0].PFaCliente[0].ID : "0");

            SetDates();
        }

        void SetDates()
        {
            var dia = DateTime.Now.Day.ToString();
            var mes = DateTime.Now.Month.ToString();
            var anio = DateTime.Now.Year.ToString();

            pickerTextFieldMes.Text = DatesHelper.GetMonthName(mes);
			pickerTextFieldAnio.Text = anio;

            _pickerDataMes.SelectedIndex = int.Parse(mes) - 1;
            _pickerViewMes.Select(_pickerDataMes.SelectedIndex, 0, true);

            _main.distribucionFacturada.Mes = DatesHelper.GetMonthKey(pickerTextFieldMes.Text);
            _main.distribucionFacturada.Anio = pickerTextFieldAnio.Text;
        }
    }
}
