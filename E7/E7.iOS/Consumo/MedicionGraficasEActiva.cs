using Foundation;
using System;
using UIKit;
using SciChart.iOS.Charting;
using E7.iOS.Utils;
using E7.Services;
using System.Collections.Generic;
using E7.Models;
using E7.iOS.Utils.DataBase;
using System.Linq;
using System.Globalization;
using System.Text;

namespace E7.iOS
{
    public partial class MedicionGraficasEActiva : UIViewController
    {
        SCIChartSurface _surface;
        SCIVerticallyStackedMountainsCollection _mountainCollection;
        RestClient _client;
        LoadingOverlay _loadPop;
        public static List<BEMedicionGrafica> resultadoMedicionYGraficas { get; set; }

        public MedicionGraficasEActiva() : base("MedicionGraficasEActiva", null)
        {
        }

        public MedicionGraficasEActiva(IntPtr b) : base(b) { }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            // Perform any additional setup after loading the view, typically from a nib.

            EdgesForExtendedLayout = UIRectEdge.None;

            _client = new RestClient();
            _surface = new SCIChartSurface();
            _surface.TranslatesAutoresizingMaskIntoConstraints = true;
            _loadPop = new LoadingOverlay(UIScreen.MainScreen.Bounds);
            resultadoMedicionYGraficas = new List<BEMedicionGrafica>();

            SCIThemeManager.ApplyTheme(_surface, SCIThemeManager.SCIChart_Bright_SparkStyleKey);

            _surface.XAxes.Add(ChartCreator.xDateAxis());
            var y = ChartCreator.yAxis("Energía Activa (kWh)");
            y.LabelProvider = new ThousandsLabelProvider();
            _surface.YAxes.Add(y);

            this.View.AddSubview(_surface);

            GetReporte();
        }

        public override void ViewDidAppear(bool animated)
        {
            base.ViewDidAppear(animated);

            _surface.Frame = this.View.Bounds;
        }

        public override void DidReceiveMemoryWarning()
        {
            base.DidReceiveMemoryWarning();
            // Release any cached data, images, etc that aren't in use.
        }

        async void GetReporte()
        {
            View.Add(_loadPop);
            var main = Application.GetInstance();
            var medicionGrafica = main.medicionGrafica;
            List<BEMedicionGrafica> resultado;
            List<TBLMedicionGrafica> resultado1;
            DAReporte_Consumo conexion = new DAReporte_Consumo();
            BLTBLMedicionGrafica conexionBD = new BLTBLMedicionGrafica();
            TBLMedicionGrafica data = new TBLMedicionGrafica();
            string MensajeErrorMG;
            Alert alert = new Alert();

            var online = _client.CheckConnection();
            var _modooff = "";
            if (online)
                _modooff = "0";
            else
                _modooff = main.ModoOff;
            
            if (_modooff == "0")
            {
                resultado = await conexion.Reporte_MedidionGrafica(medicionGrafica.Desde, medicionGrafica.Hasta, medicionGrafica.ClienteID.ToString(), medicionGrafica.MedidorID.ToString());
                MensajeErrorMG = conexion.ResultadoFinalRMG;
                if (resultado == null)
                {
                    var a = alert.ShowMessage("Alerta", MensajeErrorMG);
                    a.Clicked += A_Clicked;
                    a.Show();
                }
                else if (resultado.Count == 0)
                {
                    var a = alert.ShowMessage("Alerta", "No hay datos.");
                    a.Clicked += A_Clicked;
                    a.Show();
                }
                else
                {
                    resultadoMedicionYGraficas = resultado;
                    var EnergiaActiva = resultado.OrderBy(x => x.FechayHora).Select(x => x.EnergiaActiva).ToArray();
                    var Potencia = resultado.OrderBy(x => x.FechayHora).Select(x => x.Potencia).ToArray();
                    var EnergiaReactiva = resultado.OrderBy(x => x.FechayHora).Select(x => x.EnergiaReactiva).ToArray();
                    var FechayHora = resultado.OrderBy(x => x.FechayHora).Select(x => x.FechayHora).ToArray();
                    var Tension = resultado.OrderBy(x => x.FechayHora).Select(x => x.Tension).ToArray();
                    var Corriente = resultado.OrderBy(x => x.FechayHora).Select(x => x.Corriente).ToArray();

                    var ds1 = new XyDataSeries<DateTime, double> { SeriesName = "Energía Activa (kWh)" };

                    resultadoMedicionYGraficas.Clear();
                    for (var i = 0; i < EnergiaActiva.Count(); i++)
                    {
                        ds1.Append(FechayHora[i], double.Parse(EnergiaActiva[i]));
                        resultadoMedicionYGraficas.Add(new BEMedicionGrafica()
                        {
                            FechayHora = FechayHora[i],
                            EnergiaActiva = EnergiaActiva[i],
                            Potencia = Potencia[i],
                            EnergiaReactiva = EnergiaReactiva[i],
                            Tension = Tension[i],
                            Corriente = Corriente[i]
                        });
                    }

                    var series1 = ChartCreator.GetMountainRenderableSeries(ds1, new SCISolidPenStyle(UIColor.FromRGB(47, 174, 249), 2), new SCISolidBrushStyle(UIColor.FromRGBA(47, 174, 249, 150)));

                    _mountainCollection = new SCIVerticallyStackedMountainsCollection();
                    _mountainCollection.Add(series1);

                    _surface.RenderableSeries.Add(_mountainCollection);
                    _surface.ChartModifiers.Add(new SCICursorModifier());
                    _surface.ChartModifiers.Add(new SCIPinchZoomModifier()
                    {
                        Direction = SCIDirection2D.XDirection,
                    });
                    _surface.ChartModifiers.Add(new SCIZoomExtentsModifier());
                    _surface.ChartModifiers.Add(new SCIXAxisDragModifier());

                    _loadPop.Hide();
                }
            }
            else if (_modooff != "0")
            {
                data.CodCliente = medicionGrafica.ClienteID.ToString();
                data.CodMedicion = medicionGrafica.MedidorID.ToString();
                DateTime fechaD = DateTime.ParseExact(medicionGrafica.Desde, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                DateTime fechaH = DateTime.ParseExact(medicionGrafica.Hasta, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                resultado1 = await conexionBD.Reporte_MedicionGrafica(data);
                MensajeErrorMG = "Error al momento de leer los datos guardados";
                if (resultado1 == null)
                {
                    var a = alert.ShowMessage("Alerta", MensajeErrorMG);
                    a.Clicked += A_Clicked;
                    a.Show();
                }
                else if (resultado1.Count == 0)
                {
                    var a = alert.ShowMessage("Alerta", "No hay datos sincronizados.");
                    a.Clicked += A_Clicked;
                    a.Show();
                }
                else
                {
                    var list = Encoding.UTF8.GetString(resultado1[0].BusquedaJson);
                    resultado = (List<BEMedicionGrafica>)Newtonsoft.Json.JsonConvert.DeserializeObject<List<BEMedicionGrafica>>(list);

                    var _ultimoMes = resultado.Where(x => (x.FechayHora <= fechaD)).ToArray();
                    var EnergiaActiva = resultado.OrderBy(z => z.FechayHora).Where(y => y.FechayHora > fechaD && y.FechayHora < fechaH).Select(x => x.EnergiaActiva).ToArray();
                    var Potencia = resultado.OrderBy(z => z.FechayHora).Where(y => y.FechayHora > fechaD && y.FechayHora < fechaH).Select(x => x.Potencia).ToArray();
                    var EnergiaReactiva = resultado.OrderBy(z => z.FechayHora).Where(y => y.FechayHora > fechaD && y.FechayHora < fechaH).Select(x => x.EnergiaReactiva).ToArray();
                    var Tension = resultado.OrderBy(z => z.FechayHora).Where(y => y.FechayHora > fechaD && y.FechayHora < fechaH).Select(x => x.Tension).ToArray();
                    var Corriente = resultado.OrderBy(z => z.FechayHora).Where(y => y.FechayHora > fechaD && y.FechayHora < fechaH).Select(x => x.Corriente).ToArray();

                    DateTime[] FechayHora = resultado.OrderBy(z => z.FechayHora).Where(y => y.FechayHora > fechaD && y.FechayHora < fechaH).Select(x => x.FechayHora).ToArray();

                    if (FechayHora.Count() > 0 && _ultimoMes.Count() > 0)
                    {
                        var ds1 = new XyDataSeries<DateTime, double> { SeriesName = "Energía Activa (kWh)" };

                        resultadoMedicionYGraficas.Clear();
                        for (var i = 0; i < EnergiaActiva.Count(); i++)
                        {
                            ds1.Append(FechayHora[i], double.Parse(EnergiaActiva[i]));
                            resultadoMedicionYGraficas.Add(new BEMedicionGrafica()
                            {
                                FechayHora = FechayHora[i],
                                EnergiaActiva = EnergiaActiva[i],
                                Potencia = Potencia[i],
                                EnergiaReactiva = EnergiaReactiva[i],
                                Tension = Tension[i],
                                Corriente = Corriente[i]
                            });
                        }

                        var series1 = ChartCreator.GetMountainRenderableSeries(ds1, new SCISolidPenStyle(UIColor.FromRGB(47, 174, 249), 2), new SCISolidBrushStyle(UIColor.FromRGBA(47, 174, 249, 150)));

                        _mountainCollection = new SCIVerticallyStackedMountainsCollection();
                        _mountainCollection.Add(series1);

                        _surface.RenderableSeries.Add(_mountainCollection);
                        _surface.ChartModifiers.Add(new SCICursorModifier());
                        _surface.ChartModifiers.Add(new SCIPinchZoomModifier()
                        {
                            Direction = SCIDirection2D.XDirection
                        });
                        _surface.ChartModifiers.Add(new SCIZoomExtentsModifier());
                        _surface.ChartModifiers.Add(new SCIXAxisDragModifier());

                        _loadPop.Hide();
                    }
                    else
                    {
                        string _meses = "";
                        if (_modooff == "1") { _meses = " mes."; } else { _meses = " meses."; }

                        var a = alert.ShowMessage("Alerta", "Usted solo tiene datos sincronizados de " + _modooff + _meses);
                        a.Clicked += (sender, e) => {
                            EnergiaActiva = resultado.OrderBy(z => z.FechayHora).Select(x => x.EnergiaActiva).ToArray();
                            Potencia = resultado.OrderBy(z => z.FechayHora).Select(x => x.Potencia).ToArray();
                            EnergiaReactiva = resultado.OrderBy(z => z.FechayHora).Select(x => x.EnergiaReactiva).ToArray();
                            Tension = resultado.OrderBy(z => z.FechayHora).Select(x => x.Tension).ToArray();
                            Corriente = resultado.OrderBy(z => z.FechayHora).Select(x => x.Corriente).ToArray();
                            FechayHora = resultado.OrderBy(z => z.FechayHora).Select(x => x.FechayHora).ToArray();

                            var ds1 = new XyDataSeries<DateTime, double> { SeriesName = "Energía Activa (kWh)" };

                            resultadoMedicionYGraficas.Clear();
                            for (var i = 0; i < EnergiaActiva.Count(); i++)
                            {
                                ds1.Append(FechayHora[i], double.Parse(EnergiaActiva[i]));
                                resultadoMedicionYGraficas.Add(new BEMedicionGrafica()
                                {
                                    FechayHora = FechayHora[i],
                                    EnergiaActiva = EnergiaActiva[i],
                                    Potencia = Potencia[i],
                                    EnergiaReactiva = EnergiaReactiva[i],
                                    Tension = Tension[i],
                                    Corriente = Corriente[i]
                                });
                            }

                            var series1 = ChartCreator.GetMountainRenderableSeries(ds1, new SCISolidPenStyle(UIColor.FromRGB(47, 174, 249), 2), new SCISolidBrushStyle(UIColor.FromRGBA(47, 174, 249, 150)));

                            _mountainCollection = new SCIVerticallyStackedMountainsCollection();
                            _mountainCollection.Add(series1);

                            _surface.RenderableSeries.Add(_mountainCollection);
                            _surface.ChartModifiers.Add(new SCICursorModifier());
                            _surface.ChartModifiers.Add(new SCIPinchZoomModifier()
                            {
                                Direction = SCIDirection2D.XDirection
                            });
                            _surface.ChartModifiers.Add(new SCIZoomExtentsModifier());
                            _surface.ChartModifiers.Add(new SCIXAxisDragModifier());

                            _loadPop.Hide();
                        };
                        a.Show();
                    }
                }
            }
        }

        void A_Clicked(object sender, UIButtonEventArgs e)
        {
            this.NavigationController.PopViewController(true);
        }
    }
}