// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace E7.iOS
{
    [Register ("ConsumoFacturadoController")]
    partial class ConsumoFacturadoController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel lblAnios { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel lblClientes { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel lblFacturacion { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField pickerTextField { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField pickerTextFieldAnios { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField pickerTextFieldFacturacion { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIStackView stackPrincipal { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView stackPrincipalBg { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView txtTitle { get; set; }

        [Action ("UIButton19447_TouchUpInside:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void UIButton19447_TouchUpInside (UIKit.UIButton sender);

        void ReleaseDesignerOutlets ()
        {
            if (lblAnios != null) {
                lblAnios.Dispose ();
                lblAnios = null;
            }

            if (lblClientes != null) {
                lblClientes.Dispose ();
                lblClientes = null;
            }

            if (lblFacturacion != null) {
                lblFacturacion.Dispose ();
                lblFacturacion = null;
            }

            if (pickerTextField != null) {
                pickerTextField.Dispose ();
                pickerTextField = null;
            }

            if (pickerTextFieldAnios != null) {
                pickerTextFieldAnios.Dispose ();
                pickerTextFieldAnios = null;
            }

            if (pickerTextFieldFacturacion != null) {
                pickerTextFieldFacturacion.Dispose ();
                pickerTextFieldFacturacion = null;
            }

            if (stackPrincipal != null) {
                stackPrincipal.Dispose ();
                stackPrincipal = null;
            }

            if (stackPrincipalBg != null) {
                stackPrincipalBg.Dispose ();
                stackPrincipalBg = null;
            }

            if (txtTitle != null) {
                txtTitle.Dispose ();
                txtTitle = null;
            }
        }
    }
}