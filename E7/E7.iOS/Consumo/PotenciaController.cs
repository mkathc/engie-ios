using Foundation;
using System;
using UIKit;
using SciChart.iOS.Charting;
using E7.iOS.Utils;
using E7.Services;

namespace E7.iOS
{
    public partial class PotenciaController : UIViewController
    {
        SCIChartSurface _surface;
        SCIVerticallyStackedColumnsCollection _columnCollection;
        RestClient _client;

        public PotenciaController() : base("PotenciaController", null)
        {
        }

        public PotenciaController(IntPtr b) : base(b) { }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            // Perform any additional setup after loading the view, typically from a nib.

            EdgesForExtendedLayout = UIRectEdge.None;
            _client = new RestClient();
            _surface = new SCIChartSurface();

            SCIThemeManager.ApplyTheme(_surface, SCIThemeManager.SCIChart_Bright_SparkStyleKey);
			_surface.TranslatesAutoresizingMaskIntoConstraints = true;

            var x = ChartCreator.xAxis();
            x.LabelProvider = new MonthLabelProvider();
            _surface.XAxes.Add(x);

            var y = ChartCreator.yAxis("Potencia (kW)");
            y.LabelProvider = new MillionsLabelProvider();
            _surface.YAxes.Add(y);

			this.View.AddSubview(_surface);

            GetReporte();
        }

        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);

            _surface.Frame = this.View.Bounds;
        }

        public override void DidReceiveMemoryWarning()
        {
            base.DidReceiveMemoryWarning();
            // Release any cached data, images, etc that aren't in use.
        }

        void GetReporte()
        {
            if (EnergiaActivaController.resultadoConsumoFacturado == null)
                return;
            
            var resultado = EnergiaActivaController.resultadoConsumoFacturado;

            var ds1 = new XyDataSeries<double, double> { SeriesName = "Potencia (kW)", AcceptUnsortedData = true };

            foreach (var x in resultado)
            {
                ds1.Append(double.Parse(x.Mes), double.Parse(x.Potencia));
            }

            var series1 = ChartCreator.GetColumnRenderableSeries(ds1, new SCISolidPenStyle(UIColor.FromRGB(47, 174, 249), 2), new SCISolidBrushStyle(UIColor.FromRGB(47, 174, 249)));
            _columnCollection = new SCIVerticallyStackedColumnsCollection();

            _columnCollection.Add(series1);

            _surface.RenderableSeries.Add(_columnCollection);
            _surface.ChartModifiers.Add(new SCICursorModifier());
        }
    }
}