// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace E7.iOS
{
    [Register ("MedicionGraficasController")]
    partial class MedicionGraficasController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel lblClientes { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel lblDesde { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel lblHasta { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel lblMedidor { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField pickerTextFieldClientes { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField pickerTextFieldDesdeAnio { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField pickerTextFieldDesdeDia { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField pickerTextFieldDesdeMes { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField pickerTextFieldHastaAnio { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField pickerTextFieldHastaDia { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField pickerTextFieldHastaMes { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField pickerTextFieldMedidor { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIStackView stackPrincipal { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView stackPrincipalBg { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView txtTitle { get; set; }

        [Action ("UIButton34579_TouchUpInside:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void UIButton34579_TouchUpInside (UIKit.UIButton sender);

        void ReleaseDesignerOutlets ()
        {
            if (lblClientes != null) {
                lblClientes.Dispose ();
                lblClientes = null;
            }

            if (lblDesde != null) {
                lblDesde.Dispose ();
                lblDesde = null;
            }

            if (lblHasta != null) {
                lblHasta.Dispose ();
                lblHasta = null;
            }

            if (lblMedidor != null) {
                lblMedidor.Dispose ();
                lblMedidor = null;
            }

            if (pickerTextFieldClientes != null) {
                pickerTextFieldClientes.Dispose ();
                pickerTextFieldClientes = null;
            }

            if (pickerTextFieldDesdeAnio != null) {
                pickerTextFieldDesdeAnio.Dispose ();
                pickerTextFieldDesdeAnio = null;
            }

            if (pickerTextFieldDesdeDia != null) {
                pickerTextFieldDesdeDia.Dispose ();
                pickerTextFieldDesdeDia = null;
            }

            if (pickerTextFieldDesdeMes != null) {
                pickerTextFieldDesdeMes.Dispose ();
                pickerTextFieldDesdeMes = null;
            }

            if (pickerTextFieldHastaAnio != null) {
                pickerTextFieldHastaAnio.Dispose ();
                pickerTextFieldHastaAnio = null;
            }

            if (pickerTextFieldHastaDia != null) {
                pickerTextFieldHastaDia.Dispose ();
                pickerTextFieldHastaDia = null;
            }

            if (pickerTextFieldHastaMes != null) {
                pickerTextFieldHastaMes.Dispose ();
                pickerTextFieldHastaMes = null;
            }

            if (pickerTextFieldMedidor != null) {
                pickerTextFieldMedidor.Dispose ();
                pickerTextFieldMedidor = null;
            }

            if (stackPrincipal != null) {
                stackPrincipal.Dispose ();
                stackPrincipal = null;
            }

            if (stackPrincipalBg != null) {
                stackPrincipalBg.Dispose ();
                stackPrincipalBg = null;
            }

            if (txtTitle != null) {
                txtTitle.Dispose ();
                txtTitle = null;
            }
        }
    }
}