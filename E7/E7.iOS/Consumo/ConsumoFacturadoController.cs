﻿using System;
using System.Drawing;
using E7.Services;
using E7.iOS.Utils;
using UIKit;
using E7.Models;
using System.Collections.Generic;

namespace E7.iOS
{
    public partial class ConsumoFacturadoController : UIViewController
    {
        PickerDataModel _pickerDataClientes;
		UIPickerView _pickerViewClientes;
        PickerDataModel _pickerDataAnios;
        UIPickerView _pickerViewAnios;
        PickerDataModel _pickerDataFacturacion;
        UIPickerView _pickerViewFacturacion;
        Application _main;

        public ConsumoFacturadoController() : base("ConsumoFacturadoController", null)
        {
        }

        public ConsumoFacturadoController(IntPtr b) : base(b) {}

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            // Perform any additional setup after loading the view, typically from a nib.

            if (this.View.Frame.Height <= 480)
            {
                NSLayoutConstraint.Create(txtTitle, NSLayoutAttribute.Top,
                                            NSLayoutRelation.Equal, this.View, NSLayoutAttribute.Top,
                                            1, 82).Active = true;
                NSLayoutConstraint.Create(stackPrincipalBg, NSLayoutAttribute.Top,
                                          NSLayoutRelation.Equal, this.View, NSLayoutAttribute.Top,
                                          1, 115).Active = true;
                NSLayoutConstraint.Create(stackPrincipal, NSLayoutAttribute.Top,
                                            NSLayoutRelation.Equal, this.View, NSLayoutAttribute.Top,
                                            1, 125).Active = true;
                lblClientes.Font = lblClientes.Font.WithSize(10);
                lblFacturacion.Font = lblClientes.Font.WithSize(10);
                lblAnios.Font = lblClientes.Font.WithSize(10);
            }

            _main = Application.GetInstance();
            _main.consumoFacturado = new ConsumoFacturadoModel();
            GetClientes();
        }

        void _pickerData_ValueChanged(object sender, EventArgs e)
        {
            pickerTextField.Text = _pickerDataClientes.SelectedItem;
            _main.consumoFacturado.Cliente = _pickerDataClientes.SelectedItem;

            _pickerDataFacturacion.Items.Clear();
            foreach(var x in _main.clientes)
            {
                if (x.NomCliente == _pickerDataClientes.SelectedItem)
                {
                    _main.consumoFacturado.ClienteID = int.Parse(x.CodCliente);
					_main.consumoFacturado.PuntoFID = 0;
                    foreach(var y in x.PFaCliente)
                    {
                        _pickerDataFacturacion.Items.Add(y.Descripcion);
                    }
                }
            }
            _pickerDataFacturacion.SelectedIndex = 0;
            pickerTextFieldFacturacion.Text = _pickerDataFacturacion.SelectedItem;
        }

        void _pickerDataAnios_ValueChanged(object sender, EventArgs e)
        {
            pickerTextFieldAnios.Text = _pickerDataAnios.SelectedItem;
            _main.consumoFacturado.Anio = _pickerDataAnios.SelectedItem;
        }

        void _pickerDataFacturacion_ValueChanged(object sender, EventArgs e)
        {
            pickerTextFieldFacturacion.Text = _pickerDataFacturacion.SelectedItem;
            _main.consumoFacturado.PuntoF = _pickerDataFacturacion.SelectedItem;

            foreach (var x in _main.clientes)
                foreach (var y in x.PFaCliente)
                    if (y.Descripcion == _pickerDataFacturacion.SelectedItem)
                        _main.consumoFacturado.PuntoFID = int.Parse(y.ID);
        }

        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);
            NavigationController.NavigationBar.Translucent = true;
        }

        public override void DidReceiveMemoryWarning()
        {
            base.DidReceiveMemoryWarning();
            // Release any cached data, images, etc that aren't in use.
        }

        partial void UIButton19447_TouchUpInside(UIButton sender)
        {
            NavigationItem.BackBarButtonItem = new UIBarButtonItem("", UIBarButtonItemStyle.Plain, null);
        }

        void DonePicker()
        {
            
        }

        void GetClientes()
        {
            var main = Application.GetInstance();
            var clientes = main.clientes;

            _pickerDataClientes = new PickerDataModel();
            _pickerDataFacturacion = new PickerDataModel();
            _pickerDataAnios = new PickerDataModel();

            foreach(var c in clientes)
                _pickerDataClientes.Items.Add(c.NomCliente);
            
            if (clientes[0] != null)
                foreach (var x in clientes[0].PFaCliente)
                    _pickerDataFacturacion.Items.Add(x.Descripcion);

            _pickerDataClientes.SelectedIndex = 0;
            pickerTextField.Text = _pickerDataClientes.SelectedItem;
            _pickerDataClientes.ValueChanged += _pickerData_ValueChanged;
            _pickerViewClientes = new TextField(pickerTextField);
            _pickerViewClientes.Model = _pickerDataClientes;
			pickerTextField.InputView = _pickerViewClientes;

            _pickerDataFacturacion.SelectedIndex = 0;
            pickerTextFieldFacturacion.Text = _pickerDataFacturacion.SelectedItem;
            _pickerDataFacturacion.ValueChanged += _pickerDataFacturacion_ValueChanged;
            _pickerViewFacturacion = new TextField(pickerTextFieldFacturacion);
            _pickerViewFacturacion.Model = _pickerDataFacturacion;
            pickerTextFieldFacturacion.InputView = _pickerViewFacturacion;

            foreach (var x in Application.Anios)
                _pickerDataAnios.Items.Add(x);
            _pickerDataAnios.SelectedIndex = 0;
            pickerTextFieldAnios.Text = _pickerDataAnios.SelectedItem;
            _pickerDataAnios.ValueChanged += _pickerDataAnios_ValueChanged;
            _pickerViewAnios = new TextField(pickerTextFieldAnios);
            _pickerViewAnios.Model = _pickerDataAnios;
            pickerTextFieldAnios.InputView = _pickerViewAnios;

            _main.consumoFacturado.ClienteID = int.Parse((clientes[0] != null) ? clientes[0].CodCliente : "0");
            _main.consumoFacturado.PuntoFID = int.Parse((clientes[0].PFaCliente[0] != null) ? clientes[0].PFaCliente[0].ID : "0");
            _main.consumoFacturado.Anio = _pickerDataAnios.Items[0];
        }
    }
}
