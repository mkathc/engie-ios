using Foundation;
using System;
using UIKit;
using E7.Services;
using E7.iOS.Utils;

namespace E7.iOS
{
    public partial class OlvideContrasenaController : KeyboardBaseController
    {
        public OlvideContrasenaController (IntPtr handle) : base (handle)
        {
        }

        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);

            tfUsuario.BackgroundColor = UIColor.FromRGBA(0, 0, 0, 256);
            tfUsuario.TextColor = UIColor.White;
            tfUsuario.Layer.BorderColor = UIColor.White.CGColor;
            tfUsuario.Layer.BorderWidth = 1.5f;
            tfUsuario.Layer.CornerRadius = 13.0f;
            tfUsuario.AttributedPlaceholder = new NSAttributedString("Usuario", null, UIColor.White);

            btnCancelar.BackgroundColor = UIColor.FromRGB(2, 73, 125);
            btnCancelar.Layer.CornerRadius = 18.0f;
            btnCancelar.Layer.ShadowOpacity = 0.8f;
            btnCancelar.Layer.ShadowOffset = new CoreGraphics.CGSize(0.0f, 3.0f);

            btnRestaurar.BackgroundColor = UIColor.FromRGB(2, 73, 125);
            btnRestaurar.Layer.CornerRadius = 18.0f;
            btnRestaurar.Layer.ShadowOpacity = 0.8f;
            btnRestaurar.Layer.ShadowOffset = new CoreGraphics.CGSize(0.0f, 3.0f);
        }

        partial void BtnCancelar_TouchUpInside(UIButton sender)
        {
            this.DismissModalViewController(true);
        }

        async partial void BtnRestaurar_TouchUpInside(UIButton sender)
        {
            if (string.IsNullOrEmpty(tfUsuario.Text))
            {
                ShowMessage("Error", "Por favor ingrese un usuario");
                return;
            }
            View.Add(_loadPop);
            RandomPassword rp = new RandomPassword();
            var _codigo = await rp.Generate();

            var resultado = await _client.EnviarMailToken(tfUsuario.Text, _codigo);
            if (resultado.ValidarOperacion == "true")
            {
                var restaurarController = (CodePasswordController)Storyboard.InstantiateViewController("RestaurarContrasena");
                await PresentViewControllerAsync(restaurarController, true);
                Application.Usuario = tfUsuario.Text;
                Application.Codigo = _codigo;
                _loadPop.Hide();
            }
            else
            {
                ShowMessage("Error", "El usuario no es válido");
                _loadPop.Hide();
            }
        }
    }
}
