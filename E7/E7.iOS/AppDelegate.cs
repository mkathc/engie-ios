﻿using System;
using System.ComponentModel.Design;
using Foundation;
using UIKit;

namespace E7.iOS
{
	// The UIApplicationDelegate for the application. This class is responsible for launching the
	// User Interface of the application, as well as listening (and optionally responding) to application events from iOS.
	[Register ("AppDelegate")]
	public class AppDelegate : UIApplicationDelegate
	{
		// class-level declarations
        public override UIWindow Window
        {
            get;
            set;
        }

        public UIStoryboard MainStoryboard
        {
            get { return UIStoryboard.FromName("Main", NSBundle.MainBundle); }
        }

        public UIViewController GetViewController(UIStoryboard storyboard, string viewControllerName)
        {
            return storyboard.InstantiateViewController(viewControllerName);
        }

        public void SetRootViewController(UIViewController rootViewController, bool animate)
        {
            if (animate)
            {
                var transitionType = UIViewAnimationOptions.TransitionFlipFromRight;

                Window.RootViewController = rootViewController;
                UIView.Transition(Window, 0.5, transitionType,
                                  () => Window.RootViewController = rootViewController,
                                  null);
            }
            else
            {
                Window.RootViewController = rootViewController;
            }
        }

        public RootViewController RootViewController { get { return Window.RootViewController as RootViewController; } }

        public override bool FinishedLaunching(UIApplication application, NSDictionary launchOptions)
        {
            // create a new window instance based on the screen size
            Window = new UIWindow(UIScreen.MainScreen.Bounds);

            // If you have defined a root view controller, set it here:
            //Window.RootViewController = new RootViewController();
            //Window.RootViewController = new LoginController();

            // make the window visible
            Window.MakeKeyAndVisible();

            var loginViewController = GetViewController(MainStoryboard, "LoginController") as LoginController;
            //loginViewController.OnLoginSuccess += LoginViewController_OnLoginSuccess;
            SetRootViewController(loginViewController, false);

            //var licensingContract = "<LicenseContract>" +
            //    "<Customer>jck.9007@gmail.com</Customer>" +
            //    "<OrderId>Trial</OrderId>" +
            //    "<LicenseCount>1</LicenseCount>" +
            //    "<IsTrialLicense>true</IsTrialLicense>" +
            //    "<SupportExpires>01/05/2018 00:00:00</SupportExpires>" +
            //    "<ProductCode>SC-IOS-2D-ENTERPRISE-SRC</ProductCode>" +
            //    "<KeyCode>b7d3f0eab9f30aa6494429072ca601b9eabd8af3c7dc374718d9bc17a55b98a5db90b11bdf0e236e9b3c4ca4d9151b6a165068592b0d0eda7d4920f92bd38024fd19817fae6377c6c6b804d311edf92234e0e534ca7537a8774f856a62fd9f4a88ca5370a2dbcb6a49a8f65463eff9a8534dac2302e6909211b193fae07667769be4889ce2e2b6d91015d0b08d235723c0d70cfeef14444a5c80955ad8b4a881d94c0803bfdf8925</KeyCode>" +
            //"</LicenseContract>";
            var licensingContract = "<LicenseContract>" +
                "<Customer>Engie Energía Peru S.A.</Customer>" +
                "<OrderId>ABT171006-4035-82164</OrderId>" +
                "<LicenseCount>1</LicenseCount>" +
                "<IsTrialLicense>false</IsTrialLicense>" +
                "<SupportExpires>10/06/2018 00:00:00</SupportExpires>" +
                "<ProductCode>SC-IOS-ANDROID-2D-ENTERPRISE-SRC</ProductCode>" +
                "<KeyCode>eacf520a838012385f87b1ad8f6d0fc95a489c67679d2211b3abd16dff13e874586b326a035886f3540692eacd82f3691541102690a2ddd590c1969087691d8f904a579a296d486c962cd708769efa18c3a3f500adcb425e34b937c172c2cab5b91b2b1572a16dd9cd6c7518db512aa6b80b1f2e85091b7df1f7966a9cc739f9b9d0675a92fd58e2eef5847da3e00ae39de672024d6f3cc4b4a975ee27becd28efda88febe4886f6b1dce0f5e0f6ea18eeaa97b3eb3acd8518454c1fc8a245148dd76b27</KeyCode>" +
                "</LicenseContract>";

            SciChart.iOS.Charting.SCIChartSurface.SetRuntimeLicenseKey(licensingContract);

            return true;
        }

        //void LoginViewController_OnLoginSuccess(object sender, EventArgs e)
        //{
        //    var tabBarController = GetViewController(MainStoryboard, "RootViewController");
        //    SetRootViewController(tabBarController, true);
        //}

		public override void OnResignActivation (UIApplication application)
		{
			// Invoked when the application is about to move from active to inactive state.
			// This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) 
			// or when the user quits the application and it begins the transition to the background state.
			// Games should use this method to pause the game.
		}

		public override void DidEnterBackground (UIApplication application)
		{
			// Use this method to release shared resources, save user data, invalidate timers and store the application state.
			// If your application supports background exection this method is called instead of WillTerminate when the user quits.
		}

		public override void WillEnterForeground (UIApplication application)
		{
			// Called as part of the transiton from background to active state.
			// Here you can undo many of the changes made on entering the background.
		}

		public override void OnActivated (UIApplication application)
		{
			// Restart any tasks that were paused (or not yet started) while the application was inactive. 
			// If the application was previously in the background, optionally refresh the user interface.
		}

		public override void WillTerminate (UIApplication application)
		{
			// Called when the application is about to terminate. Save data, if needed. See also DidEnterBackground.
		}
	}
}
