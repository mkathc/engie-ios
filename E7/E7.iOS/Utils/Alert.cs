﻿using System;
using System.Threading.Tasks;
using Foundation;
using UIKit;

namespace E7.iOS.Utils
{
    public class Alert
    {
        public UIAlertView ShowMessage(string Title, string Message)
        {
            UIAlertView alert = new UIAlertView()
            {
                Title = Title,
                Message = Message
            };
            alert.AddButton("Aceptar");
            alert.Show();
            return alert;
        }

        public void ShowMessageWithButton(string Title, string Message)
        {
            UIAlertView alert = new UIAlertView()
            {
                Title = Title,
                Message = Message,
            };
            alert.AddButton("Aceptar");
            alert.Show();
        }

        public void ShowMessageAndBack(string Title, string Message)
        {
            UIAlertView alert = new UIAlertView()
            {
                Title = Title,
                Message = Message,
            };
            alert.AddButton("Aceptar");
            alert.Show();
        }

        public static Task<bool> ShowAlert(string Title, string Message)
        {
            var tcs = new TaskCompletionSource<bool>();

            UIApplication.SharedApplication.InvokeOnMainThread(new Action(() =>
            {
                UIAlertView alert = new UIAlertView(Title, Message, null, NSBundle.MainBundle.LocalizedString("Cancel", "Cancel"),
                                    NSBundle.MainBundle.LocalizedString("OK", "OK"));
                alert.Clicked += (sender, buttonArgs) => tcs.SetResult(buttonArgs.ButtonIndex != alert.CancelButtonIndex);
                alert.Show();
            }));

            return tcs.Task;
        }
    }
}
