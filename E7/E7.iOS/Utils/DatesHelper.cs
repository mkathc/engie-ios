﻿using System;
using System.Collections.Generic;
using System.Globalization;

namespace E7.iOS.Utils
{
    public class DatesHelper
    {
        public static string GetMonthKey(string clave)
        {
            Dictionary<string, string> d = new Dictionary<string, string>();
            d.Add("Ene", "01");
            d.Add("Feb", "02");
            d.Add("Mar", "03");
            d.Add("Abr", "04");
            d.Add("May", "05");
            d.Add("Jun", "06");
            d.Add("Jul", "07");
            d.Add("Ago", "08");
            d.Add("Sep", "09");
            d.Add("Oct", "10");
            d.Add("Nov", "11");
            d.Add("Dic", "12");

            return d[clave];
        }

        public static string GetMonthName(string clave)
        {
            clave = (clave.Length == 1) ? "0" + clave : clave;
            Dictionary<string, string> d = new Dictionary<string, string>();
            d.Add("00", "");
            d.Add("01", "Ene");
            d.Add("02", "Feb");
            d.Add("03", "Mar");
            d.Add("04", "Abr");
            d.Add("05", "May");
            d.Add("06", "Jun");
            d.Add("07", "Jul");
            d.Add("08", "Ago");
            d.Add("09", "Sep");
            d.Add("10", "Oct");
            d.Add("11", "Nov");
            d.Add("12", "Dic");

            return d[clave];
        }

        public static string GetMonthNameByAb(string name)
        {
            Dictionary<string, string> d = new Dictionary<string, string>();
            d.Add("Ene", "Enero");
            d.Add("Feb", "Febrero");
            d.Add("Mar", "Marzo");
            d.Add("Abr", "Abril");
            d.Add("May", "Mayo");
            d.Add("Jun", "Junio");
            d.Add("Jul", "Julio");
            d.Add("Ago", "Agosto");
            d.Add("Sep", "Septiembre");
            d.Add("Oct", "Octubre");
            d.Add("Nov", "Noviembre");
            d.Add("Dic", "Diciembre");

            return d[name];
        }

        public static bool IsDateValid(string d, string h)
        {
            DateTime desde;
            DateTime.TryParseExact(d, "dd/MM/yyyy", new CultureInfo("en-US"), DateTimeStyles.None, out desde);
            DateTime hasta;
            DateTime.TryParseExact(h, "dd/MM/yyyy", new CultureInfo("en-US"), DateTimeStyles.None, out hasta);

            if (desde <= hasta)
                return true;
            else
                return false;
        }
    }
}
