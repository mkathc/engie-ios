﻿using System.Linq;
using Xamarin.Auth;

namespace E7.iOS.Utils
{
    public class SaveAccountInfo
    {
        public void SaveCredentials(string userName, string password)
        {
            if (!string.IsNullOrWhiteSpace(userName) && !string.IsNullOrWhiteSpace(password))
            {
                Account account = new Account
                {
                    Username = userName
                };
                account.Properties.Add("Password", password);
                AccountStore.Create().Save(account, Application.AppName);
            }
        }

        public string UserName
        {
            get
            {
                var account = AccountStore.Create().FindAccountsForService(Application.AppName).FirstOrDefault();
                return (account != null) ? account.Username : null;
            }
        }

        public string Password
        {
            get
            {
                var account = AccountStore.Create().FindAccountsForService(Application.AppName).FirstOrDefault();
                return (account != null) ? account.Properties["Password"] : null;
            }
        }

        public void DeleteCredentials()
        {
            var account = AccountStore.Create().FindAccountsForService(Application.AppName).FirstOrDefault();
            if (account != null)
            {
                AccountStore.Create().Delete(account, Application.AppName);
            }
        }
    }
}
