﻿using System;
using System.Collections.Generic;
using SciChart.iOS.Charting;
using UIKit;

namespace E7.iOS.Utils
{
    public class ThousandsLabelProvider : SCINumericLabelProvider
    {
        public override string FormatLabel(SCIGenericType dataValue)
        {
            return base.FormatLabel(new SCIGenericType(dataValue.doubleData / 1000d)) + "k";
        }
    }

    public class BillionsLabelProvider : SCINumericLabelProvider
    {
        public override string FormatLabel(SCIGenericType dataValue)
        {
            return base.FormatLabel(new SCIGenericType(dataValue.doubleData / 1E9)) + "B";
        }
    }

    public class MillionsLabelProvider : SCINumericLabelProvider
    {
        public override string FormatLabel(SCIGenericType dataValue)
        {
            return base.FormatLabel(new SCIGenericType(dataValue.doubleData / 1000000d)) + "M";
        }
    }

    public class MonthLabelProvider : SCINumericLabelProvider
    {
        public override string FormatLabel(SCIGenericType dataValue)
        {
            return DatesHelper.GetMonthName(base.FormatLabel(new SCIGenericType(dataValue.intData)));
        }
    }

    public class MonthLabelProvider2 : SCIDateTimeLabelProvider
    {
        
    }

    public class ChartCreator
    {
        public static SCINumericAxis xAxis()
        {
            var axis = new SCINumericAxis()
            {
                Style = new SCIAxisStyle()
                {
                    LabelStyle = new SCITextFormattingStyle()
                    {
                        Color = UIColor.Black
                    },
                    GridBandBrush = new SCISolidBrushStyle(UIColor.White),
                    DrawMinorGridLines = false,
                    DrawMajorGridLines = false,
                },
                AutoTicks = false,
                MajorDelta = new SCIGenericType(1),
                MinorDelta = new SCIGenericType(0.2)
            };

            return axis;
        }

        public static SCINumericAxis yAxis(string axisTitle)
        {
            var axis = new SCINumericAxis()
            {
                Style = new SCIAxisStyle()
                {
                    LabelStyle = new SCITextFormattingStyle()
                    {
                        Color = UIColor.Black
                    },
                    GridBandBrush = new SCISolidBrushStyle(UIColor.White),
                    DrawMinorGridLines = false,
                    DrawMajorGridLines = false,
                },
                GrowBy = new SCIDoubleRange(0, 0.1),
                AxisTitle = axisTitle,
                AxisAlignment = SCIAxisAlignment.Left,
            };

            return axis;
        }

        public static SCIDateTimeAxis xDateAxis()
        {
            var axis = new SCIDateTimeAxis()
            {
                Style = new SCIAxisStyle()
                {
                    LabelStyle = new SCITextFormattingStyle()
                    {
                        Color = UIColor.Black
                    },
                    GridBandBrush = new SCISolidBrushStyle(UIColor.White),
                    DrawMinorGridLines = false,
                    DrawMajorGridLines = false,
                },
            };

            return axis;
        }

        // Colum series
        public static SCIStackedColumnRenderableSeries GetColumnRenderableSeries(IDataSeries dataSeries, SCISolidPenStyle strokeStyle, SCISolidBrushStyle fillBrushStyle)
        {
            return new SCIStackedColumnRenderableSeries
            {
                DataSeries = dataSeries,
                DataPointWidth = 0.8,
                StrokeStyle = strokeStyle,
                FillBrushStyle = fillBrushStyle
            };
        }

        public static SCIStackedColumnRenderableSeries GetColumnRenderableSeriesWithUint(IDataSeries dataSeries, uint strokeStyle, uint fillBrushStyle)
        {
            return new SCIStackedColumnRenderableSeries
            {
                DataSeries = dataSeries,
                DataPointWidth = 0.8,
                StrokeStyle = new SCISolidPenStyle(strokeStyle, 2),
                FillBrushStyle = new SCISolidBrushStyle(fillBrushStyle)
            };
        }

        // Mountain series
        public static SCIStackedMountainRenderableSeries GetMountainRenderableSeries(IDataSeries dataSeries, SCISolidPenStyle strokeStyle, SCISolidBrushStyle areaStyle)
        {
            return new SCIStackedMountainRenderableSeries
            {
                DataSeries = dataSeries,
                StrokeStyle = strokeStyle,
                AreaStyle = areaStyle
            };
        }
    }
}
