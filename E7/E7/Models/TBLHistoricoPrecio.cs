﻿using System;
namespace E7.Models
{
    public class TBLHistoricoPrecio
    {
        public DateTime Fecha { get; set; }
        public string CodCliente { get; set; }
        public string CodPuntoFacturacion { get; set; }
        public string Moneda { get; set; }
        public string Concepto { get; set; }
        public string Consumo { get; set; }
    }
}
