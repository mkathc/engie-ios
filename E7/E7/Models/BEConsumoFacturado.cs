﻿using System;

namespace E7.Models
{
    public class BEConsumoFacturado
    {
        public string Item { get; set; }
        public string Anio { get; set; }
        public string Mes { get; set; }
        public string EnergiaActiva { get; set; }
        public string Potencia { get; set; }
        public DateTime Fecha { get; set; }
    }
}