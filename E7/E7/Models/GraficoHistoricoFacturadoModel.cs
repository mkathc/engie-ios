﻿using System;
namespace E7.Models
{
    public class GraficoHistoricoFacturadoModel
    {
        public string Cliente { get; set; }
        public int ClienteID { get; set; }
        public string PuntoF { get; set; }
        public int PuntoFID { get; set; }
        public string Desde { get; set; }
        public string Hasta { get; set; }
        public string Moneda { get; set; }
    }
}
