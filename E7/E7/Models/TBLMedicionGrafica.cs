﻿namespace E7.Models
{
    public class TBLMedicionGrafica
    {
        public string CodCliente { get; set; }
        public string CodMedicion { get; set; }
        public byte[] BusquedaJson { get; set; }
    }
}
