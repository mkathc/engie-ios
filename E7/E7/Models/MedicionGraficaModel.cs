﻿using System;
namespace E7.Models
{
    public class MedicionGraficaModel
    {
        public string Cliente { get; set; }
        public int ClienteID { get; set; }
        public string Medidor { get; set; }
        public int MedidorID { get; set; }
        public string Desde { get; set; }
        public string Hasta { get; set; }
    }
}
