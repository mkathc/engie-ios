﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using E7.Models;
using E7.Util;
using Plugin.Connectivity;

namespace E7.Services
{
    public class RestClient
    {
        public string ResultadoFinalRCF { get; set; }
        public string ResultadoFinalRMG { get; set; }
        public string ResultadoFinalRHF { get; set; }
        public string ResultadoFinalRDF { get; set; }
        public string ResultadoFinalRHP { get; set; }
        public string ResultadoFinalEMT { get; set; }
        public string ResultadoFinalCC { get; set; }
        public string ResultadoFinalAU { get; set; }
        public string ResultadoFinalUC { get; set; }
        UTConfiguracion config = new UTConfiguracion();

        public bool CheckConnection()
        {
            if (!CrossConnectivity.IsSupported)
                return true;

            return CrossConnectivity.Current.IsConnected;
        }

        public Task<BEUsuarioLoginE7> AutenticarUsuario(string Usuario, string Clave)
        {
            return Task.Run(() =>
            {
                try
                {
                    BEUsuarioLoginE7 entidad = null;
                    using (var client = new HttpClient())
                    {
                        client.BaseAddress = new Uri(config.URLServicesLogin);
                        client.DefaultRequestHeaders.Clear();
                        client.DefaultRequestHeaders.Add(config.ApiKeyName, config.ApiKeyValues);

                        HttpResponseMessage response = client.GetAsync("AutenticarUsuario?Usuario=" + Usuario + "&Clave=" + Clave).Result;

                        if (response.IsSuccessStatusCode)
                        {
                            var res = response.Content.ReadAsStringAsync().Result;
                            entidad = Newtonsoft.Json.JsonConvert.DeserializeObject<BEUsuarioLoginE7>(res);
                        }
                    }
                    return entidad;
                }
                catch(Exception ex)
                {
                    ResultadoFinalAU = ex.Message;
                    return null;
                }
            });
            //BEUsuarioLoginE7 entidad = null;
            //using (var client = new HttpClient())
            //{
            //    client.BaseAddress = new Uri(config.URLServicesLogin);
            //    client.DefaultRequestHeaders.Clear();
            //    client.DefaultRequestHeaders.Add(config.ApiKeyName, config.ApiKeyValues);

            //    HttpResponseMessage response = await client.GetAsync("AutenticarUsuario?Usuario=" + Usuario + "&Clave=" + Clave);

            //    if (response.IsSuccessStatusCode)
            //    {
            //        var res = response.Content.ReadAsStringAsync().Result;
            //        entidad = Newtonsoft.Json.JsonConvert.DeserializeObject<BEUsuarioLoginE7>(res);
            //    }
            //}
            //return entidad;
        }

        public async Task<List<Clientes>> Usuario_Cliente(string Usuario)
        {
            List<Clientes> entidad = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(config.URLServicesLogin);
                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Add(config.ApiKeyName, config.ApiKeyValues);

                HttpResponseMessage response = await client.GetAsync("Usuario_Cliente?Usuario=" + Usuario);

                if (response.IsSuccessStatusCode)
                {
                    var res = response.Content.ReadAsStringAsync().Result;
                    entidad = (List<Clientes>)Newtonsoft.Json.JsonConvert.DeserializeObject<List<Clientes>>(res);
                }
            }
            return entidad; 
        }

        public Task<List<BEConsumoFacturado>> Reporte_ConsumoFacturado(string Anio, string CodCliente, string CodPuntoFacturacion)
        {
            //return Task.Run(() =>
            //{
            //    try
            //    {
            //        string Api = "http://appwebservice.engie-energia.pe/appmovil/Api/SReportes/";

            //        List<BEConsumoFacturado> entidad = null;
            //        using (var client = new HttpClient())
            //        {
            //            client.BaseAddress = new Uri(Api);

            //            // Realizar la petición GET
            //            HttpResponseMessage response = client.GetAsync("ReporteC_ConsumoFacturado?Anio=" + Anio + "&CodCliente=" + CodCliente + "&CodPuntoFacturacion=" + CodPuntoFacturacion).Result;

            //            if (response.IsSuccessStatusCode)
            //            {
            //                var res = response.Content.ReadAsStringAsync().Result;
            //                entidad = (List<BEConsumoFacturado>)Newtonsoft.Json.JsonConvert.DeserializeObject<List<BEConsumoFacturado>>(res);
            //            }
            //        }
            //        return entidad;
            //    }
            //    catch (Exception ex)
            //    {
            //        ResultadoFinalRCF = ex.Message;
            //        return null;
            //    }
            //});
            return Task.Run(() =>
            {
                try
                {
                    List<BEConsumoFacturado> entidad = null;
                    using (var client = new HttpClient())
                    {
                        client.BaseAddress = new System.Uri(config.URLServicesReporte);
                        client.DefaultRequestHeaders.Clear();
                        client.DefaultRequestHeaders.Add(config.ApiKeyName, config.ApiKeyValues);

                        // Realizar la petición GET
                        HttpResponseMessage response = client.GetAsync("ReporteC_ConsumoFacturado?Anio=" + Anio + "&CodCliente=" + CodCliente + "&CodPuntoFacturacion=" + CodPuntoFacturacion).Result;

                        if (response.IsSuccessStatusCode)
                        {
                            var res = response.Content.ReadAsStringAsync().Result;
                            entidad = (List<BEConsumoFacturado>)Newtonsoft.Json.JsonConvert.DeserializeObject<List<BEConsumoFacturado>>(res);
                        }
                    }
                    return entidad;
                }
                catch (Exception ex)
                {
                    ResultadoFinalRCF = ex.Message;
                    return null;
                }
            });
        }

        public Task<List<BEMedicionGrafica>> Reporte_MedidionGrafica(string FechaDesde, string FechaHasta, string CodCliente, string CodMedidor)
        {
            return Task.Run(() =>
            {
                try
                {
                    string Api = "http://appwebservice.engie-energia.pe/appmovil/Api/SReportes/";

                    List<BEMedicionGrafica> entidad = null;
                    using (var client = new HttpClient())
                    {
                        client.BaseAddress = new Uri(Api);

                        // Realizar la petición GET
                        HttpResponseMessage response = client.GetAsync("ReporteC_MedicionyGrafica?FechaDesde=" + FechaDesde + "&FechaHasta=" + FechaHasta + "&CodCliente=" + CodCliente + "&CodMedidor=" + CodMedidor).Result;

                        if (response.IsSuccessStatusCode)
                        {
                            var res = response.Content.ReadAsStringAsync().Result;
                            entidad = (List<BEMedicionGrafica>)Newtonsoft.Json.JsonConvert.DeserializeObject<List<BEMedicionGrafica>>(res);
                        }
                    }
                    return entidad;
                }
                catch (Exception ex)
                {
                    ResultadoFinalRMG = ex.Message;
                    return null;
                }
            });
        }

        public Task<List<GraficoHistoricos>> Reporte_DistribucionFacturacion(string Fecha, string CodCliente, string CodPuntoFacturacion, string Moneda)
        {
            return Task.Run(() =>
            {
                try
                {
                    List<GraficoHistoricos> entidad = null;
                    using (var client = new HttpClient())
                    {
                        client.BaseAddress = new System.Uri(config.URLServicesReporte);
                        client.DefaultRequestHeaders.Clear();
                        client.DefaultRequestHeaders.Add(config.ApiKeyName, config.ApiKeyValues);

                        // Realizar la petición GET
                        HttpResponseMessage response = client.GetAsync("ReporteF_DistribucionFacturacion?Fecha=" + Fecha + "&CodCliente=" + CodCliente + "&CodPuntoFacturacion=" + CodPuntoFacturacion + "&Moneda=" + Moneda).Result;

                        if (response.IsSuccessStatusCode)
                        {
                            var res = response.Content.ReadAsStringAsync().Result;
                            entidad = (List<GraficoHistoricos>)Newtonsoft.Json.JsonConvert.DeserializeObject<List<GraficoHistoricos>>(res);
                        }
                    }
                    return entidad;
                }
                catch (Exception ex)
                {
                    ResultadoFinalRHP = ex.Message;
                    return null;
                }
            });
        }

        public Task<List<BEGraficosFacturacion>> Reporte_HistoricoFacturado(string FechaDesde, string FechaHasta, string CodCliente, string CodPuntoFacturacion, string Moneda)
        {
            return Task.Run(() =>
            {
                try
                {
                    string Api = "http://appwebservice.engie-energia.pe/appmovil/Api/SReportes/";

                    List<BEGraficosFacturacion> entidad = null;
                    using (var client = new HttpClient())
                    {
                        client.BaseAddress = new System.Uri(Api);

                        // Realizar la petición GET
                        HttpResponseMessage response = client.GetAsync("ReporteF_HistoricoFacturado?FechaDesde=" + FechaDesde + "&FechaHasta=" + FechaHasta + "&CodCliente=" + CodCliente + "&CodPuntoFacturacion=" + CodPuntoFacturacion + "&Moneda=" + Moneda).Result;

                        if (response.IsSuccessStatusCode)
                        {
                            var res = response.Content.ReadAsStringAsync().Result;
                            entidad = (List<BEGraficosFacturacion>)Newtonsoft.Json.JsonConvert.DeserializeObject<List<BEGraficosFacturacion>>(res);
                        }
                    }
                    return entidad;
                }
                catch (Exception ex)
                {
                    ResultadoFinalRHF = ex.Message;
                    return null;
                }
            }); 
        }

        public Task<List<BEGraficosFacturacion>> Reporte_HistoricoPrecio(string FechaDesde, string FechaHasta, string CodCliente, string CodPuntoFacturacion, string Moneda)
        {
            return Task.Run(() =>
            {
                try
                {
                    string Api = "http://appwebservice.engie-energia.pe/appmovil/Api/SReportes/";

                    List<BEGraficosFacturacion> entidad = null;
                    using (var client = new HttpClient())
                    {
                        client.BaseAddress = new System.Uri(Api);

                        // Realizar la petición GET
                        HttpResponseMessage response = client.GetAsync("ReporteF_HistoricoPrecio?FechaDesde=" + FechaDesde + "&FechaHasta=" + FechaHasta + "&CodCliente=" + CodCliente + "&CodPuntoFacturacion=" + CodPuntoFacturacion + "&Moneda=" + Moneda).Result;

                        if (response.IsSuccessStatusCode)
                        {
                            var res = response.Content.ReadAsStringAsync().Result;
                            entidad = (List<BEGraficosFacturacion>)Newtonsoft.Json.JsonConvert.DeserializeObject<List<BEGraficosFacturacion>>(res);
                            //var n = Newtonsoft.Json.JsonConvert.SerializeObject(entidad);
                        }
                    }
                    return entidad;
                }
                catch (Exception ex)
                {
                    ResultadoFinalRHP = ex.Message;
                    return null;
                }
            }); 
        }

        public Task<BEUsuarioLoginE7> EnviarMailToken(string Usuario, string Token)
        {
            return Task.Run(() =>
            {
                try
                {
                    var Api = "http://appwebservice.engie-energia.pe/appmovil/Api/SLogin/";
                    BEUsuarioLoginE7 entidad = null;
                    using (var client = new HttpClient())
                    {
                        client.BaseAddress = new System.Uri(Api);

                        HttpResponseMessage response = client.GetAsync("EnviarMailToken?Usuario=" + Usuario + "&Token=" + Token).Result;

                        if (response.IsSuccessStatusCode)
                        {
                            var res = response.Content.ReadAsStringAsync().Result;
                            entidad = (BEUsuarioLoginE7)Newtonsoft.Json.JsonConvert.DeserializeObject<BEUsuarioLoginE7>(res);
                        }
                    }
                    return entidad;
                }
                catch (Exception ex)
                {
                    ResultadoFinalEMT = ex.Message;
                    return null;
                }
            });
        }

        public Task<BEUsuarioLoginE7> CambiarClave(string Usuario, string Clave)
        {
            #region Método Get UsuarioLogin
            return Task.Run(() =>
            {
                try
                {
                    var Api = "http://appwebservice.engie-energia.pe/appmovil/Api/SLogin/";
                    BEUsuarioLoginE7 entidad = null;
                    using (var client = new HttpClient())
                    {
                        client.BaseAddress = new System.Uri(Api);

                        // Realizar la petición GET
                        HttpResponseMessage response = client.GetAsync("CambiarClave?Usuario=" + Usuario + "&Clave=" + Clave).Result;

                        if (response.IsSuccessStatusCode)
                        {
                            var res = response.Content.ReadAsStringAsync().Result;
                            entidad = (BEUsuarioLoginE7)Newtonsoft.Json.JsonConvert.DeserializeObject<BEUsuarioLoginE7>(res);
                        }
                    }
                    return entidad;
                }
                catch (Exception ex)
                {
                    ResultadoFinalCC = ex.Message;
                    return null;
                }
            });
            #endregion
        }

    }

    public class DAReporte_Facturacion
    {
        public string ResultadoFinalRHF { get; set; }
        public string ResultadoFinalRDF { get; set; }
        public string ResultadoFinalRHP { get; set; }
        UTConfiguracion config = new UTConfiguracion();

        public Task<List<BEGraficosFacturacion>> Reporte_DistribucionFacturacion(string Fecha, string CodCliente, string CodPuntoFacturacion, string Moneda)
        {
            #region Método Get Reporte Distribucion Facturacion
            return Task.Run(() =>
            {
                try
                {
                    string Api = "http://appwebservice.engie-energia.pe/appmovil/Api/SReportes/";

                    List<BEGraficosFacturacion> entidad = null;
                    using (var client = new HttpClient())
                    {
                        client.BaseAddress = new System.Uri(Api);

                        // Realizar la petición GET
                        HttpResponseMessage response = client.GetAsync("ReporteF_DistribucionFacturacion?Fecha=" + Fecha + "&CodCliente=" + CodCliente + "&CodPuntoFacturacion=" + CodPuntoFacturacion + "&Moneda=" + Moneda).Result;

                        if (response.IsSuccessStatusCode)
                        {
                            var res = response.Content.ReadAsStringAsync().Result;
                            entidad = (List<BEGraficosFacturacion>)Newtonsoft.Json.JsonConvert.DeserializeObject<List<BEGraficosFacturacion>>(res);
                        }
                    }
                    return entidad;
                }
                catch (Exception ex)
                {
                    ResultadoFinalRDF = ex.Message;
                    return null;
                }
            });
            #endregion  
        }

        public Task<List<BEGraficosFacturacion>> Reporte_HistoricoFacturado(string FechaDesde, string FechaHasta, string CodCliente, string CodPuntoFacturacion, string Moneda)
        {
            #region Método Get Reporte Historico Facturado
            return Task.Run(() =>
            {
                try
                {

                    List<BEGraficosFacturacion> entidad = null;
                    using (var client = new HttpClient())
                    {
                        client.BaseAddress = new System.Uri(config.URLServicesReporte);
                        client.DefaultRequestHeaders.Clear();
                        client.DefaultRequestHeaders.Add(config.ApiKeyName, config.ApiKeyValues);

                        // Realizar la petición GET
                        HttpResponseMessage response = client.GetAsync("ReporteF_HistoricoFacturado?FechaDesde=" + FechaDesde + "&FechaHasta=" + FechaHasta + "&CodCliente=" + CodCliente + "&CodPuntoFacturacion=" + CodPuntoFacturacion + "&Moneda=" + Moneda).Result;

                        if (response.IsSuccessStatusCode)
                        {
                            var res = response.Content.ReadAsStringAsync().Result;
                            entidad = (List<BEGraficosFacturacion>)Newtonsoft.Json.JsonConvert.DeserializeObject<List<BEGraficosFacturacion>>(res);
                        }
                    }
                    return entidad;
                }
                catch (Exception ex)
                {
                    ResultadoFinalRHF = ex.Message;
                    return null;
                }
            });
            #endregion  
        }

        public Task<List<BEGraficosFacturacion>> Reporte_HistoricoPrecio(string FechaDesde, string FechaHasta, string CodCliente, string CodPuntoFacturacion, string Moneda)
        {
            #region Método Get Reporte Historico de Precios
            return Task.Run(() =>
            {
                try
                {
                    List<BEGraficosFacturacion> entidad = null;
                    using (var client = new HttpClient())
                    {
                        client.BaseAddress = new System.Uri(config.URLServicesReporte);
                        client.DefaultRequestHeaders.Clear();
                        client.DefaultRequestHeaders.Add(config.ApiKeyName, config.ApiKeyValues);

                        // Realizar la petición GET
                        HttpResponseMessage response = client.GetAsync("ReporteF_HistoricoPrecio?FechaDesde=" + FechaDesde + "&FechaHasta=" + FechaHasta + "&CodCliente=" + CodCliente + "&CodPuntoFacturacion=" + CodPuntoFacturacion + "&Moneda=" + Moneda).Result;

                        if (response.IsSuccessStatusCode)
                        {
                            var res = response.Content.ReadAsStringAsync().Result;
                            entidad = (List<BEGraficosFacturacion>)Newtonsoft.Json.JsonConvert.DeserializeObject<List<BEGraficosFacturacion>>(res);
                            //var n = Newtonsoft.Json.JsonConvert.SerializeObject(entidad);
                        }
                    }
                    return entidad;
                }
                catch (Exception ex)
                {
                    ResultadoFinalRHP = ex.Message;
                    return null;
                }
            });
            #endregion  
        }
    }

    public class DAReporte_Consumo
    {
        public string ResultadoFinalRCF { get; set; }
        public string ResultadoFinalRMG { get; set; }
        UTConfiguracion config = new UTConfiguracion();

        public Task<List<BEConsumoFacturado>> Reporte_ConsumoFacturado(string Anio, string CodCliente, string CodPuntoFacturacion)
        {
            #region Método Get Listar Consumo Facturado
            return Task.Run(() =>
            {
                try
                {
                    List<BEConsumoFacturado> entidad = null;
                    using (var client = new HttpClient())
                    {
                        client.BaseAddress = new System.Uri(config.URLServicesReporte);
                        client.DefaultRequestHeaders.Clear();
                        client.DefaultRequestHeaders.Add(config.ApiKeyName, config.ApiKeyValues);

                        // Realizar la petición GET
                        HttpResponseMessage response = client.GetAsync("ReporteC_ConsumoFacturado?Anio=" + Anio + "&CodCliente=" + CodCliente + "&CodPuntoFacturacion=" + CodPuntoFacturacion).Result;

                        if (response.IsSuccessStatusCode)
                        {
                            var res = response.Content.ReadAsStringAsync().Result;
                            entidad = (List<BEConsumoFacturado>)Newtonsoft.Json.JsonConvert.DeserializeObject<List<BEConsumoFacturado>>(res);
                        }
                    }
                    return entidad;
                }
                catch (Exception ex)
                {
                    ResultadoFinalRCF = ex.Message;
                    return null;
                }
            });
            #endregion  
        }

        public Task<List<BEMedicionGrafica>> Reporte_MedidionGrafica(string FechaDesde, string FechaHasta, string CodCliente, string CodMedidor)
        {
            #region Método Get Listar Consumo Facturado
            return Task.Run(() =>
            {
                try
                {
                    List<BEMedicionGrafica> entidad = null;
                    using (var client = new HttpClient())
                    {
                        client.BaseAddress = new System.Uri(config.URLServicesReporte);
                        client.DefaultRequestHeaders.Clear();
                        client.DefaultRequestHeaders.Add(config.ApiKeyName, config.ApiKeyValues);

                        // Realizar la petición GET
                        HttpResponseMessage response = client.GetAsync("ReporteC_MedicionyGrafica?FechaDesde=" + FechaDesde + "&FechaHasta=" + FechaHasta + "&CodCliente=" + CodCliente + "&CodMedidor=" + CodMedidor).Result;

                        if (response.IsSuccessStatusCode)
                        {
                            var res = response.Content.ReadAsStringAsync().Result;
                            entidad = (List<BEMedicionGrafica>)Newtonsoft.Json.JsonConvert.DeserializeObject<List<BEMedicionGrafica>>(res);
                        }
                    }
                    return entidad;
                }
                catch (Exception ex)
                {
                    ResultadoFinalRMG = ex.Message;
                    return null;
                }
            });
            #endregion  
        }
    }
}
