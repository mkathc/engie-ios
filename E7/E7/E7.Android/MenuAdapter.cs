﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace E7.Droid
{
    public class MenuAdapter : BaseAdapter
    {
        private Context c;
        private JavaList<ContentMenu> menu;
        private LayoutInflater inflater;

        public MenuAdapter(Context c, JavaList<ContentMenu> menu)
        {
            //(c, Android.Resource.Layout.SimpleSpinnerDropDownItem, clientes);
            this.c = c;
            this.menu = menu;
        }
        public override Java.Lang.Object GetItem(int position)
        {
            return menu.Get(position);
        }
        public override long GetItemId(int position)
        {
            return position;
        }
        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            if (inflater == null)
            {
                inflater = (LayoutInflater)c.GetSystemService(Context.LayoutInflaterService);
            }
            if (convertView == null)
            {
                convertView = inflater.Inflate(Resource.Layout.resource_menu, parent, false);
            }

            ImageView icon = convertView.FindViewById<ImageView>(Resource.Id.icon);
            TextView title = convertView.FindViewById<TextView>(Resource.Id.title);
            icon.SetImageResource(menu[position].Icono);
            title.Text = menu[position].Titulo;
            return convertView;
        }
        public override int Count
        {
            get { return menu.Size(); }
        }
    }
}