﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using V7Toolbar = Android.Support.V7.Widget.Toolbar;
using Android.Support.V7.App;
using Android.Support.V4.Widget;
using Android.Support.Design.Widget;


namespace E7.Droid
{
    [Activity(Label = "Cons2Activity", Theme = "@style/Theme.DesignDemo")]
    public class Cons2Activity : AppCompatActivity
    {
        DrawerLayout drawerLayout;
        NavigationView navigationView;
        ClientesAdapter adapter_c;
        MedidorAdapter adapter_m;
        JavaList<Clientes> clientes;
        JavaList<Medidor> medidores;
        Spinner CodCliente, CodMedidor;
        ImageButton Graficar, _dateSelectButton, _dateSelectButton1;
        Android.App.ProgressDialog progress;
        string _usuarioInterno,_codCliente, _codMedidor, _fechaD, _fechaH;

        TextView _dateDisplay, _dateDisplay1;
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            // Create your application here

            //Listar Clientes y Medidor
            _usuarioInterno = Intent.GetStringExtra("_usuarioInterno") ?? "Data not available";
            COLogin oLogin = new COLogin();
            clientes = oLogin.Usuario_Cliente(_usuarioInterno);

            SetContentView(layoutResID: Resource.Layout.cons2);
            var toolbar = FindViewById<V7Toolbar>(Resource.Id.toolbar);
            SetSupportActionBar(toolbar);
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);
            SupportActionBar.SetDisplayShowTitleEnabled(false);
            SupportActionBar.SetHomeButtonEnabled(true);
            SupportActionBar.SetHomeAsUpIndicator(Resource.Drawable.menu);
            drawerLayout = FindViewById<DrawerLayout>(Resource.Id.drawer_layout);
            navigationView = FindViewById<NavigationView>(Resource.Id.nav_view);


            _dateDisplay = FindViewById<TextView>(Resource.Id.editText3);
            _dateSelectButton = FindViewById<ImageButton>(Resource.Id.imageButton2);
            _dateDisplay1 = FindViewById<TextView>(Resource.Id.editText4);
            _dateSelectButton1 = FindViewById<ImageButton>(Resource.Id.imageButton3);
            _dateSelectButton.Click += DateSelect_OnClick;
            _dateSelectButton1.Click += DateSelect1_OnClick;

            CodCliente = FindViewById<Spinner>(Resource.Id.spinner1);
            CodMedidor = FindViewById<Spinner>(Resource.Id.spinner2);
            Graficar = FindViewById<ImageButton>(Resource.Id.imageButton1);
            CodCliente.ItemSelected += new EventHandler<AdapterView.ItemSelectedEventArgs>(CodCliente_ItemSelected);
            CodMedidor.ItemSelected += new EventHandler<AdapterView.ItemSelectedEventArgs>(CodMedidor_ItemSelected);

            Graficar.Click += delegate
            {
                progress = new Android.App.ProgressDialog(this);
                progress.Indeterminate = true;
                progress.SetProgressStyle(Android.App.ProgressDialogStyle.Horizontal);
                progress.SetMessage("Loading... Please wait...");
                progress.SetCancelable(true);
                progress.Show();
            };


            Graficar.Click += (sender, e) => Graficos();

            ListarClientes();
            Fecha();
            void Graficos()
            {
                _fechaD = _dateDisplay.Text;
                _fechaH = _dateDisplay1.Text;

                if (!string.IsNullOrEmpty(_codCliente) && !string.IsNullOrEmpty(_codMedidor) && !string.IsNullOrEmpty(_fechaD) && !string.IsNullOrEmpty(_fechaH))
                {
                    var intent_data = new Intent(this, typeof(_Consumo2Activity));
                    intent_data.PutExtra("_usuarioInterno", _usuarioInterno);
                    intent_data.PutExtra("_codCliente", _codCliente);
                    intent_data.PutExtra("_codMedidor", _codMedidor); 
                    intent_data.PutExtra("_fechaD", _fechaD);
                    intent_data.PutExtra("_fechaH", _fechaH);
                    StartActivity(intent_data);
                }
                else if (string.IsNullOrEmpty(_codCliente))
                {
                    string toast = "Seleccione el cliente";
                    Toast.MakeText(this, toast, ToastLength.Short).Show();
                }
                else if (string.IsNullOrEmpty(_codMedidor))
                {
                    string toast = "Seleccione el medidor";
                    Toast.MakeText(this, toast, ToastLength.Short).Show();
                }
                else if (string.IsNullOrEmpty(_fechaD))
                {
                    string toast = "Seleccione la fecha Desde";
                    Toast.MakeText(this, toast, ToastLength.Short).Show();
                }
                else
                {
                    string toast = "Seleccione la fecha Hasta";
                    Toast.MakeText(this, toast, ToastLength.Short).Show();
                }
            }

            void Fecha()
            {
                var time = DateTime.Now;
                _dateDisplay.Text = String.Format("01/{0}/{1}",time.Month,time.Year);
                _dateDisplay1.Text = String.Format("{0}/{1}/{2}",time.Day, time.Month, time.Year);
            }
            void ListarClientes()
            {
                _codMedidor = null;
                var _clientes = clientes;
                adapter_c = new ClientesAdapter(this, clientes);
                CodCliente.Adapter = adapter_c;
            }

            void CodCliente_ItemSelected(object sender, AdapterView.ItemSelectedEventArgs e)
            {
                _codCliente = clientes[e.Position].CodCliente;
                ListarMedidor(_codCliente);

            }
            void ListarMedidor(string _codCliente)
            {
                var _medidores = clientes.Where(x => x.CodCliente == _codCliente).Select(x => x.MedCliente).ToList();

                foreach (JavaList<Medidor> item in _medidores)
                {
                    medidores = item;
                }

                adapter_m = new MedidorAdapter(this, medidores);
                CodMedidor.Adapter = adapter_m;

            }
            void CodMedidor_ItemSelected(object sender, AdapterView.ItemSelectedEventArgs e)
            {
                _codMedidor = medidores[e.Position].Codigo;
            }
            void DateSelect_OnClick(object sender, EventArgs eventArgs)
            {
                DatePickerFragment frag = DatePickerFragment.NewInstance(delegate (DateTime time)
                {
                    _dateDisplay.Text = time.ToString("dd/MM/yyyy");
                    time.ToShortDateString();
                });
                frag.Show(FragmentManager, DatePickerFragment.TAG);
            }
            void DateSelect1_OnClick(object sender, EventArgs eventArgs)
            {
                DatePickerFragment frag = DatePickerFragment.NewInstance(delegate (DateTime time)
                {
                    _dateDisplay1.Text = time.ToString("dd/MM/yyyy");
                });
                frag.Show(FragmentManager, DatePickerFragment.TAG);
            }
        }


        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Android.Resource.Id.Home:
                    drawerLayout.OpenDrawer(Android.Support.V4.View.GravityCompat.Start);
                    return true;
            }
            return base.OnOptionsItemSelected(item);
        }
    }
}