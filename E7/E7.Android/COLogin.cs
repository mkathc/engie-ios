using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

using System.Net.Http;

namespace E7.Droid
{
    public class COLogin
    {
        public BEUsuarioLogin AutenticarUsuario(string Usuario, string Clave)
        {
            #region M�todo Get UsuarioLogin
            string Api = "http://appwebservice.engie-energia.pe/appmovil/Api/SLogin/";

            BEUsuarioLogin entidad = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Api);

                // Realizar la petici�n GET
                HttpResponseMessage response = client.GetAsync("AutenticarUsuario?Usuario=" + Usuario + "&Clave=" + Clave).Result;

                if (response.IsSuccessStatusCode)
                {
                    var res = response.Content.ReadAsStringAsync().Result;
                    entidad = (BEUsuarioLogin)Newtonsoft.Json.JsonConvert.DeserializeObject<BEUsuarioLogin>(res);
                }
            }
            return entidad;
            #endregion    
        }
        public JavaList<Clientes> Usuario_Cliente(string Usuario)
        {
            #region M�todo Get UsuarioLogin
            string Api = "http://appwebservice.engie-energia.pe/appmovil/Api/SLogin/";

            JavaList<Clientes> entidad = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Api);

                // Realizar la petici�n GET
                HttpResponseMessage response = client.GetAsync("Usuario_Cliente?Usuario=" + Usuario).Result;

                if (response.IsSuccessStatusCode)
                {
                    var res = response.Content.ReadAsStringAsync().Result;
                    entidad = (JavaList<Clientes>)Newtonsoft.Json.JsonConvert.DeserializeObject<JavaList<Clientes>>(res);
                }
            }
            return entidad;
            #endregion    
        }
    }
}