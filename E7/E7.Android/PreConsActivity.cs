﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using V7Toolbar = Android.Support.V7.Widget.Toolbar;
using Android.Support.V7.App;
using Android.Support.V4.Widget;
using Android.Support.Design.Widget;
using System.Threading;

namespace E7.Droid
{
    [Activity(Label = "PreConsActivity", Theme = "@style/Theme.DesignDemo")]
    public class PreConsActivity : Fragment
    {
        DrawerLayout drawerLayout;
        NavigationView navigationView;
        Button btn1, btn2;
        int pgdurum;
        Android.App.ProgressDialog progress;

        public event EventHandler CallConsumoFacturado;

        public void OnCallConsumoFacturado()
        {
            EventHandler handler = CallConsumoFacturado;
            if (handler != null)
            {
                handler(this, new EventArgs());
            }
        }

        public event EventHandler CallMedicionYGraficas;

        public void OnCallMedicionYGraficas()
        {
            EventHandler handler = CallMedicionYGraficas;
            if (handler != null)
            {
                handler(this, new EventArgs());
            }
        }

        public override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
        }
       
        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            return inflater.Inflate(Resource.Layout.precons, container, false);
        }

        public override void OnActivityCreated(Bundle savedInstanceState)
        {
            base.OnActivityCreated(savedInstanceState);

            //string _usuarioInterno = Intent.GetStringExtra("_usuarioInterno") ?? "Data not available";

            //var toolbar = FindViewById<V7Toolbar>(Resource.Id.toolbar);
            //SetSupportActionBar(toolbar);
            //SupportActionBar.SetDisplayHomeAsUpEnabled(true);
            //SupportActionBar.SetDisplayShowTitleEnabled(false);
            //SupportActionBar.SetHomeButtonEnabled(true);
            //SupportActionBar.SetHomeAsUpIndicator(Resource.Drawable.menu);
            //drawerLayout = FindViewById<DrawerLayout>(Resource.Id.drawer_layout);
            //navigationView = FindViewById<NavigationView>(Resource.Id.nav_view);

            //btnf = FindViewById<Button>(Resource.Id.buttonf1);
            btn2 = View.FindViewById<Button>(Resource.Id.button2);
            btn1 = View.FindViewById<Button>(Resource.Id.button1);


            //btnf.Click += (sender, e) => PreFacturacion();
            //btn1.Click += delegate
            //{
            //    //progress = new Android.App.ProgressDialog(this);
            //    progress.Indeterminate = true;
            //    progress.SetProgressStyle(Android.App.ProgressDialogStyle.Horizontal);
            //    progress.SetMessage("Loading... Please wait...");
            //    progress.SetCancelable(true);
            //    progress.Show();
            //};

            //btn2.Click += delegate
            //{
            //    //progress = new Android.App.ProgressDialog(this);
            //    progress.Indeterminate = true;
            //    progress.SetProgressStyle(Android.App.ProgressDialogStyle.Horizontal);
            //    progress.SetMessage("Loading... Please wait...");
            //    progress.SetCancelable(true);
            //    progress.Show();
            //};

            btn1.Click += (sender, e) => Cons2();

            btn2.Click += (sender, e) => Cons1();
            //void PreFacturacion()
            //{
            //    var intent_data = new Intent(this, typeof(PreFacActivity));
            //    intent_data.PutExtra("_usuarioInterno", _usuarioInterno);
            //    StartActivity(intent_data);
            //}

            void Cons1()
            {
                //var intent_data = new Intent(this, typeof(Cons1Activity));
                //intent_data.PutExtra("_usuarioInterno", _usuarioInterno);
                //StartActivity(intent_data);
                OnCallConsumoFacturado();
            }
            void Cons2()
            {
                //var intent_data = new Intent(this, typeof(Cons2Activity));
                //intent_data.PutExtra("_usuarioInterno", _usuarioInterno);
                //StartActivity(intent_data);
                OnCallMedicionYGraficas();
            }
        }
    }
}