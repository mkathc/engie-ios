﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using SciChart.Charting.Visuals.RenderableSeries;

namespace E7.Droid
{
    class ChartTypeModel
    {
        public ChartTypeModel(StackedSeriesCollectionBase seriesCollection, string header)
        {
            SeriesCollection = seriesCollection;
            TypeName = new Java.Lang.String(header);
        }

        public StackedSeriesCollectionBase SeriesCollection { get; }
        public Java.Lang.String TypeName { get; }
    }
}