﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace E7.Droid
{
    public abstract class BaseFragment : Fragment
    {
        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            return inflater.Inflate(ExampleLayoutId, null);
        }

        public abstract int ExampleLayoutId { get; }

        public override void OnStart()
        {
            base.OnStart();

            InitExample();
        }

        protected abstract void InitExample();

        public virtual void InitExampleForUiTest()
        {
        }
    }
}