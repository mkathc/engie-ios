﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using SciChart.Charting.Visuals.RenderableSeries;
using SciChart.Charting.Model.DataSeries;
using V7Toolbar = Android.Support.V7.Widget.Toolbar;
using SciChart.Drawing.Common;
using Android.Support.Design.Widget;
using Android.Support.V4.View;
using Android.Support.V7.App;
using Android.Support.V4.Widget;

namespace E7.Droid
{
    [Activity(Label = "", Theme = "@style/Theme.DesignDemo")]
    public class _Consumo1Activity : AppCompatActivity
    {
        DrawerLayout drawerLayout;
        NavigationView navigationView;
        string _usuarioInterno;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout._Consumo1);
            var toolbar = FindViewById<V7Toolbar>(Resource.Id.toolbar);
            SetSupportActionBar(toolbar);
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);

            // Create your application here

            TabLayout TabLayout = FindViewById<TabLayout>(Resource.Id.tabLayout);
            ViewPager ViewPager = FindViewById<ViewPager>(Resource.Id.viewpager);

            IList<ChartTypeModel> _chartTypesSource = new List<ChartTypeModel>();
            //Cargar Datos

            _usuarioInterno = Intent.GetStringExtra("_usuarioInterno") ?? "Data not available";
            string _codCliente = Intent.GetStringExtra("_codCliente") ?? "Data not available";
            string _anio = Intent.GetStringExtra("_anio") ?? "Data not available";
            string _codPuntoF = Intent.GetStringExtra("_codPuntoF") ?? "Data not available";

            //string _anio = "2017";
            //string _codCliente = "7";
            //string _codPuntoF = "0";

            List<BEConsumoFacturado> resultado;
            COReporte_Consumo conexion = new COReporte_Consumo();

            resultado = conexion.Reporte_ConsumoFacturado(_anio, _codCliente, _codPuntoF);

            var EnergiaActiva = resultado.Select(x => (Convert.ToDouble(x.EnergiaActiva))).ToArray();
            var Potencia = resultado.Select(x => (Convert.ToDouble(x.Potencia))).ToArray();
            var Mes = resultado.Select(x => (Convert.ToDouble(x.Mes))).ToArray();

            _chartTypesSource.Add(ChartTypeModelFactory.EnergiaActivaStackedColumn(this,EnergiaActiva, Mes, 0xff7cb5ec, 0xff7cb5ec));
            _chartTypesSource.Add(ChartTypeModelFactory.PotenciaStackedLine(Potencia, Mes, 0xff7cb5ec));


            //this line fixes swiping lag of the viewPager by caching the pages
            ViewPager.OffscreenPageLimit = 2;
            ViewPager.Adapter = new ViewPagerCons1Adapter(BaseContext, _chartTypesSource);
            TabLayout.SetupWithViewPager(ViewPager);
        }
        //Toolbar Back Button
        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            if (item.ItemId == Android.Resource.Id.Home) ;
            {
                Finish();
                var intent_data = new Intent(this, typeof(Cons1Activity));
                intent_data.PutExtra("_usuarioInterno", _usuarioInterno);
                StartActivity(intent_data);
            }
            return base.OnOptionsItemSelected(item);
        }
    }
}
