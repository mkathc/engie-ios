﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using V7Toolbar = Android.Support.V7.Widget.Toolbar;
using Android.Support.V7.App;
using Android.Support.V4.Widget;
using Android.Support.Design.Widget;

namespace E7.Droid
{
    [Activity(Label = "PreFacActivity", Theme = "@style/Theme.DesignDemo")]
    public class PreFacActivity : Fragment
    {
        DrawerLayout drawerLayout;
        NavigationView navigationView;
        Button btn1, btn2, btn3;
        Android.App.ProgressDialog progress;

        public event EventHandler CallDistribucionFacturada;

        public void OnCallDistribucionFacturada()
        {
            EventHandler handler = CallDistribucionFacturada;
            if (handler != null)
            {
                handler(this, new EventArgs());
            }
        }

        public event EventHandler CallGraficoHistoricoFacturado;

        public void OnCallGraficoHistoricoFacturado()
        {
            EventHandler handler = CallGraficoHistoricoFacturado;
            if (handler != null)
            {
                handler(this, new EventArgs());
            }
        }

        public event EventHandler CallGraficoHistoricoPrecios;

        public void OnCallGraficoHistoricoPrecios()
        {
            EventHandler handler = CallGraficoHistoricoPrecios;
            if (handler != null)
            {
                handler(this, new EventArgs());
            }
        }

        public override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            // Create your application here

            //string _usuarioInterno = Intent.GetStringExtra("_usuarioInterno") ?? "Data not available";

            //SetContentView(layoutResID: Resource.Layout.prefac);
            //var toolbar = FindViewById<V7Toolbar>(Resource.Id.toolbar);
            //SetSupportActionBar(toolbar);
            //SupportActionBar.SetDisplayHomeAsUpEnabled(true);
            //SupportActionBar.SetDisplayShowTitleEnabled(false);
            //SupportActionBar.SetHomeButtonEnabled(true);
            //SupportActionBar.SetHomeAsUpIndicator(Resource.Drawable.menu);
            //drawerLayout = FindViewById<DrawerLayout>(Resource.Id.drawer_layout);
            //navigationView = FindViewById<NavigationView>(Resource.Id.nav_view);

            //btn1 = View.FindViewById<Button>(Resource.Id.button1);
            //btn2 = View.FindViewById<Button>(Resource.Id.button2);
            //btn3 = View.FindViewById<Button>(Resource.Id.button3);
            ////var btn1 = FindViewById<ImageButton>(Resource.Id.imageButton1);
            ////var btn2 = FindViewById<ImageButton>(Resource.Id.imageButton3);
            ////var btn3 = FindViewById<ImageButton>(Resource.Id.imageButton2);
            ////btn3 = FindViewById<Button>(Resource.Id.button6);

            ////btnc.Click += (sender, e) => Precosumo();
            //////btnc.Click += (sender, e) => PreFacturacion();
            //btn1.Click += delegate
            //{
            //    progress = new Android.App.ProgressDialog(this);
            //    progress.Indeterminate = true;
            //    progress.SetProgressStyle(Android.App.ProgressDialogStyle.Horizontal);
            //    progress.SetMessage("Loading... Please wait...");
            //    progress.SetCancelable(true);
            //    progress.Show();
            //};
            //btn1.Click += (sender, e) => fac1();
            //btn2.Click += delegate
            //{
            //    progress = new Android.App.ProgressDialog(this);
            //    progress.Indeterminate = true;
            //    progress.SetProgressStyle(Android.App.ProgressDialogStyle.Horizontal);
            //    progress.SetMessage("Loading... Please wait...");
            //    progress.SetCancelable(true);
            //    progress.Show();
            //};
            //btn2.Click += (sender, e) => fac2();
            //btn3.Click += delegate
            //{
            //    progress = new Android.App.ProgressDialog(this);
            //    progress.Indeterminate = true;
            //    progress.SetProgressStyle(Android.App.ProgressDialogStyle.Horizontal);
            //    progress.SetMessage("Loading... Please wait...");
            //    progress.SetCancelable(true);
            //    progress.Show();
            //};
            //btn3.Click += (sender, e) => fac3();


            ////void PreConsumo()
            ////{
            ////    StartActivity(typeof(PreConsActivity));
            ////}
            ////void Precosumo()
            ////{
            ////    var intent_data = new Intent(this, typeof(PreConsActivity));
            ////    intent_data.PutExtra("_usuarioInterno", _usuarioInterno);
            ////    StartActivity(intent_data);
            ////}

            //void fac1()
            //{
            //    var intent_data = new Intent(this, typeof(fac2));
            //    intent_data.PutExtra("_usuarioInterno", _usuarioInterno);
            //    StartActivity(intent_data);
            //}
            //void fac2()
            //{
            //    var intent_data = new Intent(this, typeof(fac3));
            //    intent_data.PutExtra("_usuarioInterno", _usuarioInterno);
            //    StartActivity(intent_data);
            //}
            //void fac3()
            //{
            //    var intent_data = new Intent(this, typeof(fac1));
            //    intent_data.PutExtra("_usuarioInterno", _usuarioInterno);
            //    StartActivity(intent_data);
            //}
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            return inflater.Inflate(Resource.Layout.prefac, container, false);
        }

        public override void OnActivityCreated(Bundle savedInstanceState)
        {
            base.OnActivityCreated(savedInstanceState);

            btn1 = View.FindViewById<Button>(Resource.Id.button1);
            btn2 = View.FindViewById<Button>(Resource.Id.button2);
            btn3 = View.FindViewById<Button>(Resource.Id.button3);
            btn1.Click += (sender, e) => fac1();
            btn2.Click += (sender, e) => fac2();
            btn3.Click += (sender, e) => fac3();

            void fac1()
            {
                OnCallDistribucionFacturada();
            }
            void fac2()
            {
                OnCallGraficoHistoricoFacturado();
            }
            void fac3()
            {
                OnCallGraficoHistoricoPrecios();
            }
        }
    }
}