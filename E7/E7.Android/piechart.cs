﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using V7Toolbar = Android.Support.V7.Widget.Toolbar;
using Android.Support.V7.App;
using Android.Support.V4.Widget;
using Android.Support.Design.Widget;
using Android.Widget;
using Android.OS;
using OxyPlot;
using OxyPlot.Series;
using OxyPlot.Xamarin.Android;

namespace E7.Droid
{
    [Activity(Label = "", Theme = "@style/Theme.DesignDemo")]
    public class piechart : AppCompatActivity
    {
        DrawerLayout drawerLayout;
        NavigationView navigationView;
        private PlotView plotViewModel;
        private LinearLayout mLLayoutModel;
        public PlotModel MyModel { get; set; }

        //private int[] modelAllocValues = new int[] { 13, 25, 25, 20, 30, 10, 40 };
        //private int[] modelAllocValues = new int[] { 12, 5, 2, 20, 60, 1 };
        //private string[] modelAllocations = new string[] { "Slice1", "Slice2", "Slice3", "Slice4", "Slice5", "Slice6","Prueba" };
        string[] colors = new string[] { "#7DA137", "#6EA6F3", "#999999", "#3B8DA5", "#F0BA22", "#EC8542", "#808080", "#d1d1d0" };
        int total = 163;
        string _usuarioInterno;
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.piechart);
            var toolbar = FindViewById<V7Toolbar>(Resource.Id.toolbar);
            SetSupportActionBar(toolbar);
            //SupportActionBar.SetHomeButtonEnabled(true);
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);
            //drawerLayout = FindViewById<DrawerLayout>(Resource.Id.drawer_layout);
            //navigationView = FindViewById<NavigationView>(Resource.Id.nav_view);
            plotViewModel = FindViewById<PlotView>(Resource.Id.plotViewModel);
            mLLayoutModel = FindViewById<LinearLayout>(Resource.Id.linearLayoutModel);

            //Model Allocation Pie char
            var plotModel2 = new PlotModel();
            var pieSeries2 = new PieSeries();
            pieSeries2.InsideLabelPosition = 0.0;
            pieSeries2.InsideLabelFormat = null;
            pieSeries2.StrokeThickness = .25;
            pieSeries2.InsideLabelPosition = .25;

            _usuarioInterno = Intent.GetStringExtra("_usuarioInterno") ?? "Data not available";
            string _codCliente = Intent.GetStringExtra("_codCliente") ?? "Data not available";
            string _codPuntoF = Intent.GetStringExtra("_codPuntoF") ?? "Data not available";
            string _fecha = Intent.GetStringExtra("_fecha") ?? "Data not available";

            //string _fecha = "02/02/2017";
            //string _codCliente = "7";
            //string _codPuntoF = "0";
            string _moneda = "USD";

            List<GraficoHistoricos> resultado;
            COReporte_Facturacion conexion = new COReporte_Facturacion();

            resultado = conexion.Reporte_DistribucionFacturacion(_fecha, _codCliente, _codPuntoF, _moneda);

            var modelAllocValues1 = resultado.Select(x => Convert.ToDouble(x.Consumo)).ToArray();
            var modelAllocations1 = resultado.Select(x => x.Concepto).ToArray();
            var total1 = resultado.Sum(x => Convert.ToDouble(x.Consumo));


            for (int i = 0; i < modelAllocations1.Length && i < modelAllocValues1.Length && i < colors.Length; i++)
            {

                pieSeries2.Slices.Add(new PieSlice(modelAllocations1[i], modelAllocValues1[i]) { Fill = OxyColor.Parse(colors[i]) });
                pieSeries2.OutsideLabelFormat = null;

                double mValue = modelAllocValues1[i];
                double percentValue = (mValue / total1) * 100;
                string percent = percentValue.ToString("#.##");

                //Add horizontal layout for titles and colors of slices
                LinearLayout hLayot = new LinearLayout(this);
                hLayot.Orientation = Android.Widget.Orientation.Horizontal;
                LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WrapContent, LinearLayout.LayoutParams.WrapContent);
                hLayot.LayoutParameters = param;

                //Add views with colors
                LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(17, 17);

                View mView = new View(this);
                lp.TopMargin = 1; 
                mView.LayoutParameters = lp;
                mView.SetBackgroundColor(Android.Graphics.Color.ParseColor(colors[i]));

                //Add titles
                TextView label = new TextView(this);
                label.TextSize = 14;
                label.SetTextColor(Android.Graphics.Color.Black);
                label.Text = string.Join(" ", modelAllocations1[i]);
                param.LeftMargin = 8;
                label.LayoutParameters = param;

                hLayot.AddView(mView);
                hLayot.AddView(label);
                mLLayoutModel.AddView(hLayot);

            }

            plotModel2.Series.Add(pieSeries2);
            MyModel = plotModel2;
            plotViewModel.Model = MyModel;

        }


        public override bool OnOptionsItemSelected(IMenuItem item)
        {

            if (item.ItemId == Android.Resource.Id.Home) ;
            {
                Finish();
                var intent_data = new Intent(this, typeof(fac2));
                intent_data.PutExtra("_usuarioInterno", _usuarioInterno);
                StartActivity(intent_data);
            }
            return base.OnOptionsItemSelected(item);
        }
    } }