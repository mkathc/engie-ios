﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using SciChart.Charting.Visuals;
using Android.Support.Design.Widget;
using Android.Support.V4.View;
using SciChart.Charting.Visuals.RenderableSeries;
using SciChart.Drawing.Common;
using SciChart.Charting.Model.DataSeries;
using V7Toolbar = Android.Support.V7.Widget.Toolbar;
using SciChart.Charting.Visuals.Axes;
using SciChart.Charting.Modifiers;
using Android.Support.V7.App;
using Android.Support.V4.Widget;

namespace E7.Droid
{
    [Activity(Label = "", Theme = "@style/Theme.DesignDemo")]
    public class _Consumo2Activity : AppCompatActivity
    {
        DrawerLayout drawerLayout;
        NavigationView navigationView;
        string _usuarioInterno;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout._Consumo2);

            var toolbar = FindViewById<V7Toolbar>(Resource.Id.toolbar);
            SetSupportActionBar(toolbar);
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);

            // Create your application here


            TabLayout TabLayout = FindViewById<TabLayout>(Resource.Id.tabLayout);
            ViewPager ViewPager = FindViewById<ViewPager>(Resource.Id.viewpager);

            IList<ChartTypeModel> _chartTypesSource = new List<ChartTypeModel>();
            //Cargar Datos


            _usuarioInterno = Intent.GetStringExtra("_usuarioInterno") ?? "data not available";
            string _fechaD = Intent.GetStringExtra("_fechaD") ?? "data not available";
            string _fechaH = Intent.GetStringExtra("_fechaH") ?? "data not available";
            string _codCliente = Intent.GetStringExtra("_codCliente") ?? "data not available";
            string _codMedidor = Intent.GetStringExtra("_codMedidor") ?? "data not available";

            //string _fechaD = "01/08/2017";
            //string _fechaH = "31/08/2017";
            //string _codCliente = "7";
            //string _codMedidor = "0";

            List<BEMedicionGrafica> resultado;
            COReporte_Consumo conexion = new COReporte_Consumo();

            resultado = conexion.Reporte_MedidionGrafica(_fechaD, _fechaH, _codCliente, _codMedidor);

            var EnergiaActiva = resultado.Select(x => (Convert.ToDouble(x.EnergiaActiva))).ToArray();
            var Potencia = resultado.Select(x => (Convert.ToDouble(x.Potencia))).ToArray();
            var EnergiaReactiva = resultado.Select(x => (Convert.ToDouble(x.EnergiaReactiva))).ToArray();
            var FechayHora = resultado.Select(x => (Convert.ToDouble(x.FechayHora))).ToArray();

            _chartTypesSource.Add(ChartTypeModelFactory.EnergiaActivaStackedMountains(this, EnergiaActiva, FechayHora, false, 0xff7cb5ec, 0xffffffff));
            _chartTypesSource.Add(ChartTypeModelFactory.PotenciaStackedMountains(this, Potencia, FechayHora, false, 0xff7cb5ec, 0xffffffff));
            _chartTypesSource.Add(ChartTypeModelFactory.EnergiaReactivaStackedMountains(this, EnergiaReactiva, FechayHora, false, 0xff7cb5ec, 0xffffffff));

            //this line fixes swiping lag of the viewPager by caching the pages
            ViewPager.OffscreenPageLimit = 3;
            ViewPager.Adapter = new ViewPagerCons2Adapter(BaseContext, _chartTypesSource);
            TabLayout.SetupWithViewPager(ViewPager);
        }

        //Toolbar Back Button
        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            if (item.ItemId == Android.Resource.Id.Home) ;
            {
                Finish();
                var intent_data = new Intent(this, typeof(Cons2Activity));
                intent_data.PutExtra("_usuarioInterno", _usuarioInterno);
                StartActivity(intent_data);
            }
            return base.OnOptionsItemSelected(item);
        }
    }
}
