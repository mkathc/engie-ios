﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using V7Toolbar = Android.Support.V7.Widget.Toolbar;
using Android.Support.V7.App;
using Android.Support.V4.Widget;
using Android.Support.Design.Widget;
using System.Threading;

namespace E7.Droid
{
    [Activity(Label = "InicioActivity", Theme = "@style/Theme.DesignDemo")]
    public class InicioActivity : Fragment
    {
        //DrawerLayout drawerLayout;
        //NavigationView navigationView;
        Button precons, prefac;
        TextView saludos;
        int pgdurum;
        Android.App.ProgressDialog progress;
        string _usuarioInterno;
        string _nombreCompleto;

        public event EventHandler CallConsumo;

        public void OnCallConsumo()
        {
            EventHandler handler = CallConsumo;
            if (handler != null)
            {
                handler(this, new EventArgs());
            }
        }

        public event EventHandler CallFacturacion;

        public void OnCallFacturacion()
        {
            EventHandler handler = CallFacturacion;
            if (handler != null)
            {
                handler(this, new EventArgs());
            }
        }

        public InicioActivity(string usuario, string nombre)
        {
            _usuarioInterno = usuario;
            _nombreCompleto = nombre;
        }

        public override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetHasOptionsMenu(true);
            // Create your application here

            //string _usuarioInterno = Intent.GetStringExtra("_usuarioInterno") ?? "Data not available";
            //string _nombreCompleto = Intent.GetStringExtra("_nombreCompleto") ?? "Data not available";

            //SetContentView(layoutResID: Resource.Layout.Inicio);
            //var toolbar = View.FindViewById<V7Toolbar>(Resource.Id.toolbar);
            //SetSupportActionBar(toolbar);
            //SupportActionBar.SetDisplayHomeAsUpEnabled(true);
            //SupportActionBar.SetDisplayShowTitleEnabled(false);
            //SupportActionBar.SetHomeButtonEnabled(true);
            //SupportActionBar.SetHomeAsUpIndicator(Resource.Drawable.menu);
            //drawerLayout = View.FindViewById<DrawerLayout>(Resource.Id.drawer_layout);
            //navigationView = View.FindViewById<NavigationView>(Resource.Id.nav_view);
            //precons = View.FindViewById<Button>(Resource.Id.button2);
            //prefac = View.FindViewById<Button>(Resource.Id.button1);
            //saludos = View.FindViewById<TextView>(Resource.Id.textView1);

            //saludos.Text = "Hola " + _nombreCompleto +"!";
            //saludos.Text = "Hola Jesús!";

            //precons.Click += (sender, e)  => PreConsumo();

            //prefac.Click += (sender, e) => PreFacturacion();


            //void PreConsumo()
            //{
            //    var intent_data = new Intent(this, typeof(PreConsActivity));
            //    intent_data.PutExtra("_usuarioInterno", _usuarioInterno);
            //    StartActivity(intent_data);
            //}
            //void PreFacturacion()
            //{
            //    var intent_data = new Intent(this, typeof(PreFacActivity));
            //    intent_data.PutExtra("_usuarioInterno", _usuarioInterno);
            //    StartActivity(intent_data);
            //}
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
			return inflater.Inflate(Resource.Layout.Inicio, container, false);
        }

        public override void OnActivityCreated(Bundle savedInstanceState)
        {
            base.OnActivityCreated(savedInstanceState);

            //string _usuarioInterno = Intent.GetStringExtra("_usuarioInterno") ?? "Data not available";
            //string _nombreCompleto = Intent.GetStringExtra("_nombreCompleto") ?? "Data not available";

            precons = View.FindViewById<Button>(Resource.Id.button2);
            prefac = View.FindViewById<Button>(Resource.Id.button1);
            saludos = View.FindViewById<TextView>(Resource.Id.textView1);

            saludos.Text = "Hola " + _nombreCompleto + "!";

            precons.Click += (sender, e) => PreConsumo();

            prefac.Click += (sender, e) => PreFacturacion();


            void PreConsumo()
            {
                //var intent_data = new Intent(this, typeof(PreConsActivity));
                //intent_data.PutExtra("_usuarioInterno", _usuarioInterno);
                //StartActivity(intent_data);
                OnCallConsumo();
            }
            void PreFacturacion()
            {
                //var intent_data = new Intent(this, typeof(PreFacActivity));
                //intent_data.PutExtra("_usuarioInterno", _usuarioInterno);
                //StartActivity(intent_data);
                OnCallFacturacion();
            }
        }
    }
}
