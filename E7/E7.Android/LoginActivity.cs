﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.Graphics;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace E7.Droid
{
    [Activity(Label = "LoginActivity", Theme = "@style/Theme.DesignDemo")]
    public class LoginActivity : Activity
    {
        BEUsuarioLogin resultado;

        EditText usuario, clave;
        Button ingresar, btn2;
        Android.App.ProgressDialog progress;
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            // Create your application here

            SetContentView(layoutResID: Resource.Layout.Login);

            ingresar = FindViewById<Button>(Resource.Id.button1);
            usuario = FindViewById<EditText>(Resource.Id.editText1);
            clave = FindViewById<EditText>(Resource.Id.editText2);
            btn2 = FindViewById<Button>(Resource.Id.button2);



            ingresar.Click += delegate
            {
                progress = new Android.App.ProgressDialog(this);
                progress.Indeterminate = true;
                progress.SetProgressStyle(Android.App.ProgressDialogStyle.Spinner);
                progress.SetMessage("Loading... Please wait...");
                progress.SetCancelable(true);
                progress.Show();
            };


            ingresar.Click += (sender, e) => Login();
            btn2.Click += (sender, e) => fpass();

            void Login()
            {
                COLogin conexion = new COLogin();

                if (!string.IsNullOrEmpty(usuario.Text) && !string.IsNullOrEmpty(clave.Text))
                {
                    resultado = conexion.AutenticarUsuario(usuario.Text, clave.Text);

                    if (resultado.ValidarOperacion == "true")
                    {
                        //var intent_data = new Intent(this, typeof(InicioActivity));
                        //intent_data.PutExtra("_usuarioInterno", resultado.UsuarioInterno);
                        //intent_data.PutExtra("_nombreCompleto", resultado.NombreCompleto);
                        //StartActivity(intent_data);
                        var intent = new Intent(this, typeof(PrincipalFragment));
                        intent.PutExtra("_usuarioInterno", resultado.UsuarioInterno);
                        intent.PutExtra("_nombreCompleto", resultado.NombreCompleto);
                        StartActivity(intent);
                    }
                    else
                    {
                        Toast.MakeText(this, resultado.MensajeError, ToastLength.Long).Show();
                    }
                }
                else if (string.IsNullOrEmpty(usuario.Text))
                {
                    usuario.RequestFocus();
                    string mensaje = "Ingrese el usuario";
                    Toast.MakeText(this, mensaje, ToastLength.Long).Show();
                }
                else
                {
                    clave.RequestFocus();
                    string mensaje = "Ingrese la contraseña";
                    Toast.MakeText(this, mensaje, ToastLength.Long).Show();
                }
            }


            void fpass()
            {
                StartActivity(typeof(FpassActivity));
            }
        }


        //Borde de Botones - Redondeado
        //    protected override void OnElementChanged(ElementChangedEventArgs<Xamarin.Android.Entry> e)
        //{
        //    base.OnElementChanged(e);

        //    if (this.Control == null) return;

        //    this.Control.Background = this.Resources.GetDrawable(Resource.Drawable.noBorderEditText);
        //}



    }

}