﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using SciChart.Charting.Numerics.LabelProviders;
using Java.Lang;
using SciChart.Core.Utility;

namespace E7.Droid
{
    public class ThousandsLabelProvider: NumericLabelProvider
    {
        public override ICharSequence FormatLabelFormatted(Java.Lang.IComparable dataValue)
        {
            return new Java.Lang.String(base.FormatLabelFormatted((Java.Lang.Double)(ComparableUtil.ToDouble(dataValue) / 1000d)) + "K");
        }
    }

    public class MillionsLabelProvider : NumericLabelProvider
    {
        public override ICharSequence FormatLabelFormatted(Java.Lang.IComparable dataValue)
        {
            return new Java.Lang.String(base.FormatLabelFormatted((Java.Lang.Double)(ComparableUtil.ToDouble(dataValue) / 1000000d)) + "M");
        }
    }
}